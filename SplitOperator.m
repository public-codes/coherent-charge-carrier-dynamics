% % Split operator code written by Donghwan Kim
% % written in MATLAB R2020a
% % Note the code works for dimension==2.
addpath('Src')
addpath('Src/elfun18');
tMainStart=cputime;
% tMainStart=tic;


GenerateGridPoints;


tic
TimeStep=0;
ConstructPotentials;


if AutoCorrPotNeeded==1
    AutoCorrPot;
    return;
end




PlotPot;







%%
evaltime=zeros(Nx,floor(NTimeStep/SavingPeriodAvgK)+1);
% evaltime(:,floor(TimeStep/SavingPeriodAvgK)+1)=evalk;


% return;

% psi0=Psi;
% psi=Psi;

ConstructInitialWF;

kpsi0=ifft2(Kx.*fft2(psi0));
kpsi=kpsi0;

% sum(norm(kpsi(:))^2*Dx*Dy,'all')
% kF^2
% return;

% PlotImage2D(X,Y,kpsi0,'WF')
% return;



if not(isfolder(FigureDir))
    mkdir(FigureDir)
end

% print([FigureDir DataLabel '_Pot'],'-dpng')


% % Generating a flat part of the potential
% % V=V.*smoothconnect(X,-XSize*.1,XSize*.03);
% V=V.*takesmoothcircle(X,Y,0.15*XSize/5,0.02*XSize/5);
% % V=V.*takesmoothcircle(X,Y,12.5*lambda0,1.5*lambda0);
% % save(['ModelPot'],'V')
%
% % % % % plotting contour is a little bit slow
% % if dimension==2
% %     PlotPot;
% % end
% % print(['ModelPot'],'-dpng')


% %%
% % % plotting height distribution of the potential
% edges=linspace(-max(abs(V(:))),max(abs(V(:))),50);
% histogram(V,edges,'Normalization','pdf')
% if choosePotential==2
% sigma1=A/sqrt(2);
% end
% if choosePotential==1
% sigmaDIntegrand=@(q) Ed*Ed*2*pi.*q/(2*pi)/(2*pi)*2*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1)/2;
% sigma1=sqrt(integral(@(q) sigmaDIntegrand(q),0,qmax));
% end
% hold on
% plot(edges,exp(-edges.^2/(2*sigma1^2))/(sigma1*sqrt(2*pi)))
% hold off
% legend('simulation','theory')
% if choosePotential==1
% title('deformation potential')
% else
%     if choosePotential==2
%     title('Berry potential')
%     end
% end
% return;



%%
% V=V-1i*Vimag;
% expVimag=exp(-Vimag*Dt/hbar);


% % Exponents for time propagation
if dimension==1
    Tx=@(Kx) hbar^2*K1BZ(Kx,Nx,Dkx).^2/(2*m);
    Tkin=@(Kx) hbar^2*(K1BZ(Kx,Nx,Dkx).^2)/(2*m);
    expT=exp(-1i*Tkin(Kx)*Dt/hbar);
end
if dimension==2
    if B == 0
        Tx=@(Kx,Ky) hbar^2*K1BZ(Kx,Nx,Dkx).^2/(2*m);
        Ty=@(Kx,Ky) hbar^2*K1BZ(Ky,Ny,Dky).^2/(2*m);
        Tkin=@(Kx,Ky) hbar^2*(K1BZ(Kx,Nx,Dkx).^2+K1BZ(Ky,Ny,Dky).^2)/(2*m);
        expT=exp(-1i*Tkin(Kx,Ky)*Dt/hbar);
    end
end


expTminus=exp(1i*Tkin(Kx,Ky)*Dt/hbar);

AutoCorrWF=zeros(1,NTimeStep+1);
% VHist=zeros(Ny,Nx,NTimeStep+1);


avgKx=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgKx0Kxt=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgKy=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgX=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgY=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
MSDx=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
% MSDy=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
% MSDK=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
IPR=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
% JxTotal=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
% JyTotal=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
% NormTracker=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgTkinx=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgV=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
tauKuboList=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);

eslist=16:24;
pop1=zeros(numel(eslist),floor(NTimeStep/SavingPeriodAvgK)+1);


E0=EF;
% E0=.545*Vrms;
psiE0=zeros(Ny,Nx); % Time Fourier transform of psi about energy E0


% % For given time step Dt, exp(1i*EE*Dt/hbar) will be periodic function of
% EE with period determined by DeltaEE*Dt/hbar=2*pi. Thus, the spectrum
% calculated from the autocorrelation data at every Dt is a periodic
% function of EE with the period DeltaEE. Thus, it suffices to look at the
% energy range whose width id DeltaEE=2*pi*hbar/Dt.

% EList=linspace(-pi*hbar/Dt,pi*hbar/Dt,2401);
%
% % However, if the problem of interest is in a narrow range, one can set the
% % range manually
% EList=-3:.005:3;
% % EList=-.5:.005:.5;
% EList=EList*Vrms;
% %         SpectrumSummand=zeros(1,numel(EList));
% %         Spectrum=zeros(1,numel(EList));
% %         CheckNorm=zeros(1,NTimeStep+1);

% return;


% if TDPot==1
V0=V;
% end
if UseGPU==1
    V0=gather(V0);
end


if TDPertTheo==1
    % % perturbation theory analysis
    PertTheo;
end


ax1=figure('Position',[0 0 1300 650]);


TimeStep=0;


PlotWFandStoreData;


for TimeStep=1:NTimeStep
    %         time propagator
    TimePropagator;
    PlotWFandStoreData;

    clear psik;
end



% % ShowVariousData;


tMainEnd=cputime-tMainStart