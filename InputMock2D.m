% clear; close all;

% % Switches
loadPotential=0;
ScattPopAnal=0;
nonlinSE=0;% 1 if want nonlinear Schrodinger eq.
plotMRK=1;
AutoCorrPotNeeded=0;
recording=0;
TDPot=0;
TDtuning=0;
UseGPU=0;
TDPertTheo=0;

% //fundamental constants (not likely to adjust)
hbar = 1.0545718e-34/1;
kB = 1.38064854e-23;
e = 1.60217662e-19;

HbarRatio=1;


hbar0=hbar;
hbar=hbar0*HbarRatio;


%     //material specific constants (likely to adjust)
% the following is mostly from 3D copper to make mock 2D copper
Ed = 10 * e*1; %This is some arbitrary value
% Ed = 3 * e*1;
vs = 4.7e3; % Copper
% vs = 3700; % Platinum
me = 9.10938356e-31;
m=me*1; % Copper
% m=me*13; % Platinum

rho_Cu=8940;
% rho_Pt=21.4e3;
CuRadius=135e-12;
% PtRadius=135e-12;

rho_m = rho_Cu*CuRadius*2; % 2.41e-6 for Copper. compare with graphene 7.6e-7. kind of make sense
% rho_m = rho_Cu;
% rho_m = rho_Pt*PtRadius*2; % 2.41e-6 for Copper. compare with graphene 7.6e-7. kind of make sense

TDebye = 343; % Cu
% TDebye = 240; % Pt
% TDebye = 433; % arbitrary
qD = kB*TDebye/(hbar*vs);

EF = 7.00*e; % 7eV for Cu
% EF = 9.74*e; % Pt
vF = sqrt(2*EF/m); % group velocity. me*vF=hbar*kF
kF = m*vF/hbar;

% if UseRelativeScale==1
%     kF = kFToqDover2*qD/2;
%     EF = hbar^2*kF^2/(2*m);
%     vF = sqrt(2*EF/m); % group velocity. m*vF=hbar*kF
% end





qmax = min([qD 2*kF]);

%     //simulation specific parameters (likely to adjust)
% number of  points in each direction
% nqx = 256/2;
% nqy = 256/2;
% nqx = 64*3;
% nqy = 64*3;

T = 343;
% if UseRelativeScale==1
%     T=TDebye*TRatio;
% end
% B = 1000;
B=0;

qT = kB*min([T TDebye])/(hbar*vs);
qB=kB*T/hbar/vs;
qeff=min([5*qB,qD]);






% qy=linspace(-qmax,qmax,nqy);
% phase=rand(nqx,nqy)*2*pi;


% Lx=nqx*pi/qD;
% Vol=(nqx*pi/qD)^2;




gIntRatio=7;
gN=4*pi*hbar^2/me*gIntRatio;

choosePotential=1;% 1 for defpot, 2 for Berry, 3 for single sinusoid, 4 for constant flat potential




p0=hbar*kF;
InverseTauPTIntegrand=@(q) 1/(2*p0*p0)*Ed^2.*q/(2*pi)/(2*pi)*2*hbar.*q/(rho_m*vs*vF*vF)*2*pi.*q*vF./(exp(hbar*vs.*q/kB/T)-1);
InverseTauPT=integral(InverseTauPTIntegrand,0,qmax);
% tauPT=1/InverseTauPT;
InverseTauQPTIntegrand=@(q) m/(2*pi*hbar^3*kF^3)*Ed^2.*q.^2./sqrt(1-(q/(2*kF)).^2)*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1);
% InverseTauQPTIntegrand=@(q) m/(4*pi*hbar^3*kF^3)*Ed^2.*q.^3*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1);
InverseTauQPT=integral(InverseTauQPTIntegrand,0,qmax);
tauQPT=1/InverseTauQPT;

InverseTauQPTIntegrandFock=@(q) m/(2*pi*hbar^3*kF^3)*Ed^2.*q.^2./sqrt(1-(q/(2*kF)).^2)*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1)*hbar*vs.*q/kB/T.*(1+1./(exp(hbar*vs.*q/kB/T)-1));
% InverseTauQPTIntegrandFock=@(q) m/(4*pi*hbar^3*kF^3)*Ed^2.*q.^3*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1)*hbar*vs.*q/kB/T.*(1+1./(exp(hbar*vs.*q/kB/T)-1));
InverseTauQPTFock=integral(InverseTauQPTIntegrandFock,0,qmax);

InverseTauQPTIntegrandCoherent=@(q) m/(2*pi*hbar^3*kF^3)*Ed^2.*q.^2./sqrt(1-(q/(2*kF)).^2)*hbar.*q/(rho_m*vs).*(0+1./(exp(hbar*vs.*q/kB/T)-1))*hbar*vs.*q/kB/T.*(1/2+1./(exp(hbar*vs.*q/kB/T)-1));
% InverseTauQPTIntegrandCoherent=@(q) m/(4*pi*hbar^3*kF^3)*Ed^2.*q.^3*hbar.*q/(rho_m*vs).*(0+1./(exp(hbar*vs.*q/kB/T)-1))*hbar*vs.*q/kB/T.*(1/2+1./(exp(hbar*vs.*q/kB/T)-1));
InverseTauQPTCoherent=integral(InverseTauQPTIntegrandCoherent,0,qmax);


InverseTauQPTIntegrandHighT=@(q) m/(2*pi*hbar^3*kF^3)*Ed^2.*q.^2./sqrt(1-(q/(2*kF)).^2)*hbar.*q/(rho_m*vs).*(1./(hbar*vs.*q/kB/T)-1/2);
% InverseTauQPTIntegrandHighT=@(q) m/(4*pi*hbar^3*kF^3)*Ed^2.*q.^3*hbar.*q/(rho_m*vs).*(1./(hbar*vs.*q/kB/T)-1/2);
InverseTauQPTHighT=integral(InverseTauQPTIntegrandHighT,0,qmax);

InverseTauQPTIntegrandHighTNaive=@(q) m/(2*pi*hbar^3*kF^3)*Ed^2.*q.^2./sqrt(1-(q/(2*kF)).^2)*hbar.*q/(rho_m*vs).*(1./(hbar*vs.*q/kB/T));
% InverseTauQPTIntegrandHighTNaive=@(q) m/(4*pi*hbar^3*kF^3)*Ed^2.*q.^3*hbar.*q/(rho_m*vs)./(hbar*vs.*q/kB/T);
InverseTauQPTHighTNaive=integral(InverseTauQPTIntegrandHighTNaive,0,qmax);


% InverseTauQPTIntegrandLowT=@(s) s.^3./(exp(s)-1);
InverseTauQPTIntegrandLowT=@(s) s.^5.*(1+1./(exp(s)-1))./(exp(s)-1);
% InverseTauQPTLowT=m/(2*pi*hbar^3*kF^3)*(kB/hbar*T/vs)^4*Ed^2*hbar/(rho_m*vs)*integral(InverseTauQPTIntegrandLowT,0,inf);
InverseTauQPTLowT=m/(4*pi*hbar^3*kF^3)*(kB/hbar*T/vs)^5*Ed^2*hbar/(rho_m*vs)*integral(InverseTauQPTIntegrandLowT,0,inf);

InverseTauQPTLowTInel=4*InverseTauQPTLowT;


InverseTauQPTSPIntegrand=@(q) m/(pi*hbar^3*kF)*Ed^2./sqrt(1-(q/(2*kF)).^2)*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1);
InverseTauQPTSP=integral(InverseTauQPTSPIntegrand,0,qmax);
tauQPTSP=1/InverseTauQPTSP;

% plot(linspace(0,qmax,100),InverseTauQPTSPIntegrand(linspace(0,qmax,100)))


VmsIntegrand=@(q) Ed^2*2*pi/(2*pi)/(2*pi)*2*hbar.*q.^2/(rho_m*vs)*1/2.*(0+1./(exp(hbar*vs.*q/kB/T)-1));
Vms=integral(VmsIntegrand,0,qD);% This is an analytic estimate of the mean square of the potential, mean(V(:).^2).
VrmsAnal=sqrt(Vms);% This is an analytic estimate of the rms of the potential. In our case, the average value of the potential is zero, so rms value will be std(V(:)).

dimension=2;
order=2;% order=1 for expT.*expV, order=2 for expVover2.*expT.*expVover2


% Nx=512*32; % Assumed to be an even number. 256*3 is proper for the mock parameters
% Ny=64*1;

% Nx=128;
% Ny=Nx;

Nx=512;
Ny=Nx;


Lx=Nx*2*pi/qeff/8; % sampling 8 points for the shortest wavelength
Ly=Ny*2*pi/qeff/8;
Vol=Lx*Ly;


Ttyp=hbar^2/(2*m)*(2*pi/max([Lx Ly]))^2;

WindowSize=1.;
XSize=WindowSize*Lx;
YSize=WindowSize*Ly;
% ZSize=XSize;





nqx=floor(qD/(2*pi/XSize))*2+1;
nqy=floor(qD/(2*pi/YSize))*2+1;
qx=(-floor(qD/(2*pi/XSize)):floor(qD/(2*pi/XSize))) * 2*pi/XSize;
qy=(-floor(qD/(2*pi/YSize)):floor(qD/(2*pi/YSize))) * 2*pi/YSize;
% % % phase=rand(nqx,nqy)*2*pi;
phase=rand(Ny,Nx)*2*pi;

% save('MyPhaseSet','phase')
% load('../MyPhaseSet','phase')

Dt=1e-16*.7/1;
% For normal metal propagation
    NTimeStep=min([round(tauQPT/Dt*8) 9e3]);
% NTimeStep=2e4;


%         For normal metal propagation
SavingPeriod=round(NTimeStep/40/8);
SavingPeriod2=min([5 SavingPeriod]);
SavingPeriodAvgK=min([5 SavingPeriod]);

% SavingPeriod=100;
% SavingPeriod2=50;
% SavingPeriodAvgK=50;


if B~=0
    omegaC=e*B/m;
    lB=sqrt(hbar/(e*B));% magnetic length
    NTimeStep=6000;
    SavingPeriod=10;
    SavingPeriod2=10;
    SavingPeriodAvgK=10;
end

FigureDir='data_figures/';
if choosePotential==1
    DataLabel=sprintf('QMT%dEd%dDt%dHbR%d',round(T),round(Ed/e),round(Dt/1e-17),round(HbarRatio*100));
%     if UseRelativeScale==1
%         DataLabel=sprintf('TR%03dkFR%03dTDtuning%d',round(TRatio*100),round(kFToqDover2*100),round(TDtuning*10));
%         
%         %         NTemps=60;
%         %         DataLabel=sprintf('TRExp%03dkFR%03d',round(NTemps*log(TRatio/.05)/log(10/.05)),round(kFToqDover2*100));
%         
% %         DataLabel=sprintf('W%03dNG%04dTD%dTR%03dBoomerang',round(WindowSize*100),round(Nx),TDPot,round(TRatio*100));
%     end
end

if choosePotential==2
    DataLabel=sprintf('QMT%dEd%dDt%dHbR%d',round(T),round(Ed/e),round(Dt/1e-17),round(HbarRatio*100));
end

if choosePotential==3
    DataLabel=sprintf('QMT%dA%dDt%dHbR%d',round(T),round(A/1e-24),round(Dt/1e-17),round(HbarRatio*100));
end

if choosePotential==4 || choosePotential==5
    DataLabel=sprintf('W%03dNG%04d',round(WindowSize*100),round(Nx));
end


if nonlinSE==1
    DataLabel=sprintf([DataLabel 'gIntR%d'],round(gIntRatio));
end

% JobName=DataLabel;




