function ret = dpotAnother(x,y,vs,Kx,Ky,Ed,hbar,rho_m,kB,T,phase,qD,Vol,Nx)
V0=0;
qabs=sqrt(Kx.^2+Ky.^2);
omega=vs*qabs;
for ind=1:Nx
    for ind2=1:Nx
        if qabs(ind,ind2)<qD && qabs(ind,ind2)>0
            V_Dq=Ed*sqrt(2*hbar*omega(ind,ind2)/vs/vs/rho_m/Vol)/sqrt(exp(hbar*omega(ind,ind2)/kB/T)-1)*cos(Kx(ind,ind2)*x+Ky(ind,ind2)*y+phase(ind,ind2));
            V0=V0+V_Dq;
        end
    end
end
ret=V0;
end