function ret = dpot(x,y,vs,nqx,nqy,qx,qy,Ed,hbar,rho_m,kB,T,phase,qD,Vol)
V0=0;
for ind=1:nqx
    for ind2=1:nqy
        qabs=sqrt(qx(ind)^2+qy(ind2)^2);
        if qabs<=qD && qabs>0
            omega=vs*qabs;
            V_Dq=Ed*sqrt(2*hbar*omega/vs/vs/rho_m/Vol)/sqrt(exp(hbar*omega/kB/T)-1)*cos(qx(ind)*x+qy(ind2)*y+phase(ind,ind2));
            V0=V0+V_Dq;
        end
    end
end
ret=V0;
end