x=linspace( -XSize/2, XSize/2-XSize/Nx,Nx); %Normal grid
Dx=XSize/Nx; % For deformation potential, Dx should be smaller than the minimal grid spacing pi/qD to represent the largest wavenumber lattice vibration correctly
KxSize=2*pi/Dx;
Dkx=2*pi/XSize;
% lx=[0:Nx/2-1,-Nx/2:-1];
kx=[0:Nx/2-1,-Nx/2:-1]*Dkx;
% % Monkhorst-Pack grid
% kx=([0:Nx/2-1,-Nx/2:-1]+.5)*Dkx;
if dimension==2
y=linspace( -YSize/2, YSize/2-YSize/Ny,Ny);
Dy=YSize/Ny;
KySize=2*pi/Dy;
Dky=2*pi/YSize;
ly=[0:Ny/2-1,-Ny/2:-1];
ky=[0:Ny/2-1,-Ny/2:-1]*Dky;
end
if dimension==3
z=linspace( -ZSize/2, ZSize/2-ZSize/Nz,Nz);
Dz=ZSize/Nz;
KzSize=2*pi/Dz;
Dkz=2*pi/ZSize;
kz=[0:Nz/2-1,-Nz/2:-1]*Dkz;
end
if dimension==1
    X=x;
end
if dimension==2
    [X,Y]=meshgrid(x,y);
end
if dimension==3
    [X,Y,Z]=meshgrid(x,y,z);
end

if dimension==1
    Kx=kx;
end
if dimension==2
    [Kx,Ky]=meshgrid(kx,ky);
end
if dimension==3
    [Kx,Ky,Kz]=meshgrid(kx,ky,kz);
end



if dimension==2
    %Vector potentials
    if B~=0
        Ax=-B*Y;
        Ay=0*Y;
    end
end



% Periodicl=@(l1) modOffset(l1,Nx,-Nx/2);
% Periodick=@(k1) Periodicl(k1/Dkx)*Dkx;
% Periodick2=@(k1) modOffset(k1,KxSize,-KxSize/2);  %This expression is worse than the above function due to machine precision error


K1BZ=@(kx,Nx,Dkx) modOffset(kx/Dkx,Nx,-Nx/2)*Dkx;


% KxShifted=circshift(Kx,[Ny/2 Nx/2]);
% KyShifted=circshift(Ky,[Ny/2 Nx/2]);



% temporal grid
time=(0:SavingPeriodAvgK:NTimeStep)*Dt;
tt=0:NTimeStep;
tt=tt*Dt;

% 
% [XX,TT]=meshgrid(x,time);
% 

% 
% [XXX,TTT]=meshgrid(x,tt);


% KAngleGrid=atan2(Ky,Kx);
% NBins=181;
% Binsize=2*pi/NBins;%/41
% KAngleBins=-pi:Binsize:pi;
% KAngleBinsCenter=KAngleBins(1:end-1)+Binsize/2;
% PDFAngular=zeros(floor(NTimeStep/SavingPeriod2)+1,length(KAngleBins)-1);
if ScattPopAnal==1
    ScatteredPopulation=zeros(1,floor(NTimeStep/SavingPeriod2)+1);
    ScatteredPopulation2=zeros(1,floor(NTimeStep/SavingPeriod2)+1);
    ScatteredPopulationPT=zeros(1,floor(NTimeStep/SavingPeriod2));
    ScatteredPopulation2PT=zeros(1,floor(NTimeStep/SavingPeriod2));
end


% RRadialGrid=sqrt(X.^2+Y.^2);
% NBins2=60;
% Binsize2=XSize/2/NBins2;
% RRadialBins=0:Binsize2:XSize/2;
% RRadialBinsCenter=RRadialBins(1:end-1)+Binsize2/2;
% PDFRadial=zeros(1,length(RRadialBins)-1);


% Determining font size depending on OS
if ismac==1
    FontSizeOverall=18;
    FontSizeAxes=24;
else
    if isunix==1
        FontSizeOverall=12;
        FontSizeAxes=16;
    else
        FontSizeOverall=14;
        FontSizeAxes=18;
    end
end