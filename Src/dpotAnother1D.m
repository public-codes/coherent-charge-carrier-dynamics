function ret = dpotAnother1D(x,vs,Kx,Ed,hbar,rho_m,kB,T,phase,qD,Vol,Nx)
V0=0;
qabs=abs(Kx);
omega=vs*qabs;
for ind=1:Nx
        if qabs(ind)<qD && qabs(ind)>0
            V_Dq=Ed*sqrt(2*hbar*omega(ind)/vs/vs/rho_m/Vol)/sqrt(exp(hbar*omega(ind)/kB/T)-1)*cos(Kx(ind)*x+phase(ind));
            V0=V0+V_Dq;
        end
end
ret=V0;
end