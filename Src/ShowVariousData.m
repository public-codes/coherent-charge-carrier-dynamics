% tauQ=-1/((avgKx(end)-avgKx(1))/avgKx(1)/(Dt*floor(NTimeStep/SavingPeriodAvgK)*SavingPeriodAvgK));



% % Plot and write AvgK
PlotAvgK;

% % 
tauzero=(find(avgKx<0,1)-.5-1)*SavingPeriodAvgK*Dt; %zero appears between find(avgKx<0,1) and find(avgKx<0,1)-1, so we take the average find(avgKx<0,1)-.5
tau1st=(find(diff(avgKx)>0,1)-.5-1)*SavingPeriodAvgK*Dt;


% % % % Plot and write movie of kAngleDist
% % figure;
% % for TimeStep=NTimeStep:SavingPeriod2:NTimeStep
% %     plot(KAngleBinsCenter,PDFAngular(TimeStep/SavingPeriod2+1,:),'LineWidth',2)
% %     hold on
% %     %     plot(KAngleBinsCenter,PDFAngular2(TimeStep/SavingPeriod2+1,:))
% %     if choosePotential==2
% % %         plot(KAngleBinsCenter,.1*exp(-(KAngleBinsCenter-2*asin(qphonon/(2*kF)*2*pi/7.0156)).^2/.005))
% % %            plot(KAngleBinsCenter,.3*besselj(0,qphonon/kF*pi./sin(KAngleBinsCenter/2)).*(5.5201<=abs(qphonon/kF*pi./sin(KAngleBinsCenter/2)).*(abs(qphonon/kF*pi./sin(KAngleBinsCenter/2))<=8.6537)),':','LineWidth',2)
% %             legend(['simulation result'],['prediction from',newline,'autocorrelation',newline,'(arbitrarily scaled)'],'FontSize',20)
% %             xlim([-pi pi])
% %     end
% %     hold off
% %     set(gca,'FontSize',FontSizeOverall)
% %     title(['Angular probability distribution',newline,'timestep=',num2str(TimeStep),'/',num2str(NTimeStep)],'FontSize',FontSizeAxes)
% %     xlabel('Scattering angle, 2\theta','FontSize',FontSizeAxes)
% %     ylabel('Probability','FontSize',FontSizeAxes)
% %     MTheta(TimeStep/SavingPeriod2+1)=getframe(gcf);
% % end



if ScattPopAnal==1
    % plot the population growth of certain angle
    figure;
    tt=0:SavingPeriod2:NTimeStep;
    time=tt*Dt;
    ScatteredPopulation=ScatteredPopulation-ScatteredPopulation(1);%remove integration of initial wavefunction
    
    plot(time,ScatteredPopulation,'LineWidth',2,'DisplayName','Simulation')
    %     plot(time,ScatteredPopulation./(A^2/hbar^2*time.^2),'LineWidth',2)
    set(gca,'FontSize',FontSizeOverall)
    title(['Population growth of',newline,'scattered waves'],'FontSize',FontSizeAxes)
    xlabel('t/s','FontSize',FontSizeAxes)
    ylabel('Population','FontSize',FontSizeAxes)
    
    
    
    ang=linspace(0,2*pi,NModes+1);
    ang=ang(1:end-1);%exclude duplicated mode at ang=0=2*pi
    
    Prob=zeros(1,numel(time));
    sthing=zeros(NModes,numel(time));
    for j=1:NModes
        Eni=hbar^2*( (kF+qphonon*cos(ang(j)))^2 + (qphonon*sin(ang(j)))^2 )/(2*m)-hbar^2*kF^2/(2*m);
        Vk2k1=A/sqrt(NModes)*(exp(1i*phase(j))+exp(-1i*phase(modOffset(j+NModes/2,NModes,1))))/2;
        Prob=Prob+abs(Vk2k1).^2/Eni^2*(2-2*cos(Eni*time/hbar));
        sthing(j,:)=abs(Vk2k1).^2/Eni^2*(2-2*cos(Eni*time/hbar));
    end
    
    %     plot(ang,sthing(:,10))
    
    hold on
%         plot(time,ScatteredPopulation2,'LineWidth',2,'DisplayName','Sim2=Sim')
    
%         plot(time,Prob,'LineWidth',2,'DisplayName','AnalPT')
    
    if TDPertTheo==1
        plot(time,ScatteredPopulationPT,'LineWidth',2,'DisplayName','Perturbation Theory')
    end
    %     plot(time,A^2/hbar^2*time.^2*.1,'LineWidth',2)
    if choosePotential==3
        plot(time,A^2/hbar^2*time.^2/4,'LineWidth',2)
        plot(time,sin(A/2/hbar*time).^2)
        omega21=hbar^2*((2*sqrt(3)*kF)^2-kF^2)/(2*m)/hbar;
        RabiFreq=sqrt((A/2/hbar)^2+(omega21/2)^2);
        plot(time,(A/2/hbar)^2/RabiFreq^2*sin(RabiFreq*time).^2)
    end
    %plot(time,1-ScatteredPopulation3)
    % plot(time,ScatteredPopulation4)
    hold off
    %     legend('+2\theta_B','-2\theta_B','Perturbation','Location','northwest','FontSize',FontSizeAxes)
    legend('Location','northwest','FontSize',FontSizeAxes)
    %     ylim([0 1])
    print([FigureDir DataLabel '_PopulationGrowth'],'-dpng')
end



if ScattPopAnal==1
%    save([FigureDir DataLabel '_SomeVars'],'avgKx','SavingPeriodAvgK','PDFAngular','ScatteredPopulation','ScatteredPopulation2','ScatteredPopulation3','tauk','tauQPT')
save([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT')
else
%    save([FigureDir DataLabel '_SomeVars'],'avgKx','SavingPeriodAvgK','PDFAngular','tauk','tauQPT','Vrms','EF')
save([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT','tauzero','tau1st','Vrms','EF')
end




if dimension==2 && recording==1
    v=VideoWriter([FigureDir DataLabel '_RKmovie.avi']);
    v.FrameRate=1;
    open(v);
    writeVideo(v,MRK)
    close(v)
    
    %     v2=VideoWriter([FigureDir DataLabel '_KAngleDist.avi']);
    %     open(v2);
    %     writeVideo(v2,MTheta)
    %     close(v2)
end

