
% % % if max(abs(psi),[],'all')~=0
% % % if UseGPU==1
% % %     caxis(gca,[-1 1]*.6*max(abs(gather(psi)),[],'all'))%.15
% % % else
% % % caxis(gca,[-1 1]*.6*max(abs(psi),[],'all'))%.15
% % % end
% % % end
% % % 
% % % 
% % % load('RWBcmap.mat')
% % % % colormap(myRWBcmap)

PlotImage2D(X,Y,psi,'WF');
% pbaspect([XSize YSize 1])
title(['Re(\psi) in real space',newline,'timestep=',num2str(TimeStep),'/',num2str(NTimeStep)],'FontSize',FontSizeAxes)
% xlabel('x/m','FontSize',FontSizeAxes)
% ylabel('y/m','FontSize',FontSizeAxes)

% xlim([X(1,floor(NGridPoints*.25)) X(1,floor(NGridPoints*.75))])
% ylim([Y(floor(NGridPoints*.25),1) Y(floor(NGridPoints*.75),1)])
% xlim([X(1,floor(NGridPoints*.375)) X(1,floor(NGridPoints*.625))])
% ylim([Y(floor(NGridPoints*.375),1) Y(floor(NGridPoints*.625),1)])


if B ~= 0
hold on
plot(avgX(1:floor(TimeStep/SavingPeriodAvgK)+1),avgY(1:floor(TimeStep/SavingPeriodAvgK)+1),'--k')

ang=0:pi/50:2*pi;
rc=m*vF/(e*B);%radius of circle, cyclotron radius
xc=rc*cos(ang)+rc*cos(k0theta+pi/2)+x0;%circle coordinates
yc=rc*sin(ang)+rc*sin(k0theta+pi/2)+y0;
plot(xc,yc,':m')

hold off
end


%% Plotting probability current
% hold on
% if B==0
% quiver(X(1:10:end,1:10:end),Y(1:10:end,1:10:end),Jx(1:10:end,1:10:end),Jy(1:10:end,1:10:end),'k')
% else
%     quiver(X(1:4:end,1:4:end),Y(1:4:end,1:4:end),Jx(1:4:end,1:4:end),Jy(1:4:end,1:4:end),'k')
% end
% % % plotting total probability current
% % % 1e-14 is arbitrary scaling factor
% quiver(avgX(floor(TimeStep/SavingPeriodAvgK)+1),avgY(floor(TimeStep/SavingPeriodAvgK)+1),JxTotal(floor(TimeStep/SavingPeriodAvgK)+1)*1e-14,JyTotal(floor(TimeStep/SavingPeriodAvgK)+1)*1e-14)
% hold off
