%%
if dimension==1

    % %     single wavepacket

    
    x0=-XSize/2*.0;
    sigmax=XSize*.045/1; %.045
    
    

    Normalization=(1/(pi*2*sigmax^2))^(1/4);

    k0theta=0;
    k0vec=kF*[cos(k0theta) sin(k0theta)];
    % Gaussian
%     psi=Normalization*exp(-(X-x0).^2/(4*sigmax^2)).*exp(1i*(k0vec(1)*(X-x0)));
%     psi=Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*besselj(0,kF*sqrt(X.^2+Y.^2));




    % % Plane wave
%     psi=exp(1i*(kF*(X-x0)));
    % % Constant wavefunction
    % psi=ones(Ny,Nx);


%     % % % Loading eigenstate
%     load('Estate','psiE0')
%     if length(psiE0)<=Nx
%         psi(1+(Nx-length(psiE0))/2:(Nx+length(psiE0))/2,1+(Nx-length(psiE0))/2:(Nx+length(psiE0))/2)=psiE0;
%     end
%     psiE0=zeros(Ny,Nx);

%     % % Eigenstate
%     psi=transpose(eveckIFFT2(:,20));

    % % Normalization
    psi=psi/sqrt(Dx*norm(psi(:))^2);
end



if dimension==2




    %% % %     single wavepacket

    % x0=-0.35*Lx;
    % y0=0*Lx;
    % sigmax=.03*Lx;
    % sigmay=.03*Lx;

    % x0=-30*lambda0;
    % y0=0*Lx;
    % sigmax=2.0*lambda0;
    % sigmay=2.0*lambda0;


%     % x0=-XSize/2*.1426;
%     % y0=XSize/2*.3462;
%     x0=-XSize/2*0.25;
%     % x0=-XSize/2*.25/1;
%     y0=0*Lx;
    sigmax=XSize*.045/1; %.045
    sigmay=XSize*.045/1;

%     x0=-XSize/2+2e-8;
    x0=0;
    y0=0;
%     sigmax=.5e-8;
%     sigmay=.5e-8;

    % x0=-XSize/2*.0;
    % y0=0*Lx;
%     sigmax=XSize*.3;
%     sigmay=XSize*.3;

    if B~=0
        x0=0;
        y0=-m*vF/(e*B);%cyclotron radius
        sigmax=lB/sqrt(2);
        sigmay=lB/sqrt(2);
    end

    Normalization=(1/(pi*2*sigmax^2)/(pi*2*sigmay^2))^(1/4);

    k0theta=0;
    k0vec=kF*[cos(k0theta) sin(k0theta)];
    % % % Gaussian
    psi=Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
%     psi=Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*besselj(0,kF*sqrt(X.^2+Y.^2));

    % % 1D Gaussian
%     psi=exp(-(X-x0).^2/(4*sigmax^2)).*exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));

    % % Plane wave
    % psi=exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
    % % Constant wavefunction
%     psi=ones(Ny,Nx);



% % % % Constructing random wavefunction with energy near EF
% phase2=rand(Ny,Nx)*2*pi;
% Ener=hbar^2*(Kx.^2+Ky.^2)/(2*m);
% % mypsik=(Ener<EF+kB*T*1.05).*(Ener>EF-kB*T*1.05).*sqrt(1./(exp((Ener-EF)/kB/T)+1)).*exp(-1i*Kx*XSize/2).*exp(-1i*Ky*YSize/2)*Nx*Ny.*(exp(1i*(phase2))+exp(-1i*(circshift(flip(flip(circshift(phase2,[Ny/2 Nx/2]),1),2),[-Ny/2+1 -Nx/2+1]))));
% % mypsik=sqrt(1./(exp((Ener-EF)/kB/T)+1)).*exp(-1i*Kx*XSize/2).*exp(-1i*Ky*YSize/2)*Nx*Ny.*(exp(1i*(phase2))+exp(-1i*(circshift(flip(flip(circshift(phase2,[Ny/2 Nx/2]),1),2),[-Ny/2+1 -Nx/2+1]))));
% mypsik=sqrt(1./(exp((Ener-EF)/kB/T)+1).*(1-1./(exp((Ener-EF)/kB/T)+1))).*exp(-1i*Kx*XSize/2).*exp(-1i*Ky*YSize/2)*Nx*Ny.*(exp(1i*(phase2))+exp(-1i*(circshift(flip(flip(circshift(phase2,[Ny/2 Nx/2]),1),2),[-Ny/2+1 -Nx/2+1]))));
% psi=ifft2(mypsik);
% 
% clear phase2 mypsik;


%     % % % Loading eigenstate
%     load('Estate','psiE0')
%     if length(psiE0)<=Nx
%         psi(1+(Nx-length(psiE0))/2:(Nx+length(psiE0))/2,1+(Nx-length(psiE0))/2:(Nx+length(psiE0))/2)=psiE0;
%     end
%     psiE0=zeros(Ny,Nx);


%     % % Eigenstate
% %     psi=transpose(eveckIFFT2(:,1395));
%     psi=kUnfold2rFold2D(eveck2(:,1395),Kx2,Ky2,Nx2,Ny2,XSize,YSize);

    % % Normalization
    psi=psi/sqrt(Dx*Dy*norm(psi(:))^2);



    %% % %     speed contest wavepacket
    %
    % x0=-XSize*.35;
    % y0=YSize*.25;
    % sigmax=XSize*.03;
    % sigmay=XSize*.03;
    %
    % if B~=0
    %     x0=0;
    %     y0=-m*vF/(e*B);%cyclotron radius
    %     sigmax=lB/sqrt(2);
    %     sigmay=lB/sqrt(2);
    % end
    %
    % Normalization=(1/(pi*2*sigmax^2)/(pi*2*sigmay^2))^(1/4);
    %
    % k0theta=0;
    % k0vec=kF*[cos(k0theta) sin(k0theta)];
    % psi=Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
    %
    %
    % y0=-YSize*.25;
    % psi=psi+Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
    %
    %
    % psi=exp(-(X-x0).^2/(4*sigmax^2)).*exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
    %
    % psi=psi/sqrt(Dx*Dy*norm(psi(:))^2);


    %% % % Landau eigenstate
    % NN=10; % Landau level
    % Ny=2; % quantization number for ky
    % ky=2*pi/YSize*Ny;
    % y0=hbar*ky/(e*B);
    % psi=hermiteH(NN,(Y-y0)/lB).*exp(-(Y-y0).^2/(4*sigmay^2)).*exp(1i*ky*X);
    % % psi=((Y-y0)/lB).*exp(-(Y-y0).^2/(4*sigmay^2)).*exp(1i*ky*X);
    % % % Check normalization condition. discrete version needs Dx*Dy. If
    % % sigmax or sigmay are too big, the normalization would be smaller than
    % % 1 since the ouside will be cut out by the edges of the grid
    % % Norm(1)=Dx*Dy*norm(psi(:))^2
    %
    % % %     %     write in terms of wavepacket parameters in Heller (10.34)
    % % %     Ax0=1j*hbar*1/(4*sigmax^2);
    % % %     Ay0=1j*hbar*1/(4*sigmay^2);
    % % %     px0=hbar*kF;
    % % %     py0=0;
    % % %     sx0=-1j*hbar*log(1/(pi*2*sigmax^2)^(1/4));
    % % %     sy0=-1j*hbar*log(1/(pi*2*sigmay^2)^(1/4));





    %% % %     superposition of wavepackets for total density of states calculation
    % if B~=0
    % x0List=-XSize*.25:XSize*.1:XSize*.25;
    % % y0List=-YSize*.25:YSize*.25:YSize*.25;
    % y0List=0;
    % else
    %     x0List=-XSize*.25:XSize*.1:XSize*.25;
    %     y0List=-XSize*.25:XSize*.1:XSize*.25;
    % end
    % kx0List=-kF:kF*.5:kF;
    % ky0List=-kF:kF*.5:kF;
    %
    % % dummy variable
    % k0theta=0;
    %
    %
    % sigmax=XSize*.06;
    % sigmay=XSize*.06;
    % if B~=0
    %     sigmax=lB/sqrt(2);
    %     sigmay=lB/sqrt(2);
    % end
    % Normalization=(1/(pi*2*sigmax^2)/(pi*2*sigmay^2))^(1/4);
    %
    % for x0=x0List
    %     for y0=y0List
    %         for kx0=kx0List
    %             for ky0=ky0List
    %                 if kx0^2+ky0^2<=kF^2
    %                 k0vec=[kx0 ky0];
    %                 psi=psi+Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
    %                 end
    %             end
    %         end
    %     end
    % end
    %
    % psi=psi/sqrt(Dx*Dy*norm(psi(:))^2);


    if UseGPU==1
        psi=gpuArray(psi);
    end
end
    %%
    psi0=psi;
