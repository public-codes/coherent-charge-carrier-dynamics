% % reciprocal space distribution
ax1=gca;
% contour(KxShifted,KyShifted,Zfn2,'Fill','on')



if B==0
    
    PlotImage2D(Kx,Ky,PbDistKspace,'PbK')
    

    % plot3(KxShifted,KyShifted,circshift(PbDistKspace,[Ny/2 Nx/2]))

%     hold on
    % whut=1e-80*1/hbar^2*VabsSq.*(2*pi)^4/(XSize*YSize)^2.*(sinc(hbar^2*q.^2/(2*m)*Dt*TimeStep/(2*hbar))).^2;
    % whut=1e-92*1/hbar^2.*(2*pi)^4/(XSize*YSize)^2.*(sinc(hbar^2*q.^2/(2*m)*Dt*TimeStep/(2*hbar))*Dt*TimeStep).^2;
    % whut=4e22*VabsSq;
    % surf(KxShifted,KyShifted,whut,'FaceAlpha',.1)
%     hold off
end

% % % set(gca,'FontSize',FontSizeOverall)
% % % % title(['Probability distribution in reciprocal space',newline,'timestep=',num2str(TimeStep),'/',num2str(NTimeStep)],'FontSize',FontSizeAxes)

%     title(['T/T_D=',num2str(TRatio),',2k_F/q_D=',num2str(kFToqDover2),',V_{rms}/E_F=',num2str(Vrms/EF,'%.3f')],'FontSize',FontSizeAxes)
%     xlim([-1 1]*min(3*kF,KxSize))
%     ylim([-1 1]*min(3*kF,KxSize))
    
%     xlim([-1 1]*min(3*sqrt(2*m*Vrms)/hbar,KxSize))
%     ylim([-1 1]*min(3*sqrt(2*m*Vrms)/hbar,KxSize))
%     
%     xlim([-1 1]*min(1.5*qD,KxSize))
%     ylim([-1 1]*min(1.5*qD,KxSize))
    
    
%     xlim( ([-1 1]*(kF+qD)+qD/(kF+qD)*k0vec(1)) ) % 2*(kF+qD) is the size of the window, and shift it to center of mass qD/(kF+qD)*k0vec
%     ylim( ([-1 1]*(kF+qD)+qD/(kF+qD)*k0vec(2)) )
    
    pbaspect([KxSize KySize 1])
    % caxis([0 max(abs(PbDistKspace),[],'all')])
    % xlabel('k_x/m^{-1}','FontSize',FontSizeAxes)
    % ylabel('k_y/m^{-1}','FontSize',FontSizeAxes)
    
    if B ~= 0
        hold on
        plot(avgKx(1:floor(TimeStep/SavingPeriodAvgK)+1),avgKy(1:floor(TimeStep/SavingPeriodAvgK)+1),'--k')
        
%         ang=0:pi/50:2*pi;
%         rc=m*vF/(e*B);%radius of circle
%         xc=rc*cos(ang);%circle coordinates
%         yc=rc*sin(ang);
%         plot(xc,yc+rc,':m')


xc=kF*exp(-time(1:floor(TimeStep/SavingPeriodAvgK)+1)*(1/tauQPT)).*cos(omegaC*time(1:floor(TimeStep/SavingPeriodAvgK)+1));
yc=kF*exp(-time(1:floor(TimeStep/SavingPeriodAvgK)+1)*(1/tauQPT)).*sin(omegaC*time(1:floor(TimeStep/SavingPeriodAvgK)+1));
plot(xc,yc,'--m')

        hold off
    end
    
    if choosePotential==1
        caxis([0 max(abs(PbDistKspace),[],'all')*0.02])
    end
    
    if choosePotential==2
        % caxis([0 max(abs(PbDistKspace),[],'all')*0.005])
        caxis([0 max(abs(PbDistKspace),[],'all')*.3])
    end
    
    
    % ax2=ax1;
    hold on
    % % electron circle
    % ax2=axes('Position',ax1.Position);
    % for plotting circle
    ang=0:pi/50:2*pi;
    rc=kF;%radius of circle
    xc=rc*cos(ang);%circle coordinates
    yc=rc*sin(ang);
    
    plot(xc,yc,':k','LineWidth',1.5)
    % pbaspect([KxSize KySize 1])
    % set(ax2,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');
    
%     % % Nest circle
%     % ax2=axes('Position',ax1.Position);
%     % for plotting circle
%     ang=0:pi/50:2*pi;
%     rc=sqrt(2*m*2*pi/hbar/(TimeStep*Dt)); % For nesting
%     xc=rc*cos(ang);%circle coordinates
%     yc=rc*sin(ang);
%     
%     plot(xc,yc,':m','LineWidth',1.5)
%     % pbaspect([KxSize KySize 1])
%     % set(ax2,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');
    
    
    % % to plot shaded area
    % DkF=1/sigmax*1.2;
    % rc=kF-DkF;%radius of circle
    % xc1=rc*cos(ang);%circle coordinates
    % yc1=rc*sin(ang);
    %
    % rc=kF+DkF*1.15;%radius of circle
    % xc2=rc*cos(ang);%circle coordinates
    % yc2=rc*sin(ang);
    %
    % fill([xc1 flip(xc2)],[yc1 flip(yc2)],'k','FaceAlpha',.15,'EdgeAlpha',0)
    
    % % for jjj=1.1:.1:3
    % % rc=jjj*kF;%radius of circle
    % % xc=rc*cos(ang);%circle coordinates
    % % yc=rc*sin(ang);
    % % plot(xc,yc,':k')
    % % end
    
    % % (band) Edges of reciprocal space
    %
    %
    % ax22=axes('Position',ax1.Position);
    rectangle('Position',[-1 -1 2 2]*KxSize,'EdgeColor','r')
    % pbaspect([KxSize KySize 1])
    % set(ax22,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');
    %
    
    if choosePotential==1
    % % phonon disk for deformation potential
    % ax33=axes('Position',ax1.Position);
    
    rectangle('Position',[-1 -1 2 2]*min(3*qT,qD)+[kF 0 0 0],'FaceColor',[0 1 0 0.2],'EdgeColor',[0 1 0 0.45],'Curvature',[1 1]);
    rectangle('Position',[-1 -1 2 2]*min(3*qT,qD)+[kF-KxSize 0 0 0],'FaceColor',[0 1 0 0.2],'EdgeColor',[0 1 0 0.45],'Curvature',[1 1]);
    
    
    
    rc=qD;%radius of circle
    xc=kF+rc*cos(ang);%circle coordinates
    yc=rc*sin(ang);
    
    plot(xc,yc,':g','LineWidth',1.5)
    %
    % alpha(ax33,0.5)
    % pbaspect([KxSize KySize 1])
    % set(ax33,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');
    end
    
    if choosePotential==2
        % % phonon circle for Berry potential
        %
        % for plotting circle
        ang=0:pi/50:2*pi;
        rc=qphonon;%radius of circle
        xc=rc*cos(ang);%circle coordinates
        yc=rc*sin(ang);
        
        plot(xc+k0vec(1),yc+k0vec(2),':m','LineWidth',1.5)
        pbaspect([KxSize KySize 1])
        
        
        DkF=1/sigmax*1.5;
        rc=qphonon-DkF;%radius of circle
        xc1=k0vec(1)+rc*cos(ang);%circle coordinates
        yc1=k0vec(2)+rc*sin(ang);
        
        rc=qphonon+DkF*1.;%radius of circle
        xc2=k0vec(1)+rc*cos(ang);%circle coordinates
        yc2=k0vec(2)+rc*sin(ang);
        
        fill([xc1 flip(xc2)],[yc1 flip(yc2)],'m','FaceAlpha',.15,'EdgeAlpha',0)
        
        
        % % % phonon lines for crazy potential
        % %
        % % for plotting circle
        % Modul=@(theta) 1.5*qphonon*abs(cos(1.4FontSizeOverall*theta));
        % xxx=@(theta) Modul(theta)*cos(theta)+kF;
        % yyy=@(theta) Modul(theta)*sin(theta);
        % fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
        % pbaspect([KxSize KySize 1])
        % xxx=@(theta) -Modul(theta)*cos(theta)+kF;
        % yyy=@(theta) -Modul(theta)*sin(theta);
        % fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
        % pbaspect([KxSize KySize 1])
    end
    
    
    if choosePotential==3
        % %         direction and magnitude of single cosine wave
        ax4=axes('Position',ax1.Position);
        len=linspace(-qphonon,qphonon,10);
        % rc=0.97*kF;%radius of circle
        xc=len*cos(potangle);%circle coordinates
        yc=len*sin(potangle);
        
        plot(xc+kF,yc,':m')
        pbaspect([KxSize KySize 1])
        set(ax4,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');
    end
    
    
    
    % quiver([0 0],kF*[1 0])
    
    hold off
