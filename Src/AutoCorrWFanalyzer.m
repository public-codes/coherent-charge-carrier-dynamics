% dE=2*pi/(NTimeStep*Dt);
% ESize=2*pi/Dt;
% EGrid=linspace(0,ESize,NTimeStep+1);
% EGrid=EGrid(1:end-1);


tt=0:NTimeStep;
tt=tt*Dt;

if B~=0
figure;
    plot(tt/(2*pi/omegaC),abs(AutoCorrWF))
    title('Wavefunction autocorrelation')
xlabel('t/\tau_c')
ylabel('|\langle\psi(0)|\psi(t)\rangle|')
print('AutoCorrWF','-dpng')


figure;
EList=0:.05*hbar*omegaC:70*hbar*omegaC;
Spectrum=zeros(1,numel(EList));
for idx=1:numel(EList)
    EE=EList(idx);
Spectrum(idx)=trapz(tt,AutoCorrWF.*exp(1i*EE*tt/hbar))/(NTimeStep*Dt);
end
plot(EList/(hbar*omegaC),abs(Spectrum))
title('Spectrum')
xlabel('$E/\hbar\omega_c$','Interpreter','latex')
ylabel('Spectrum')
print('SpectrumWF','-dpng')

% % arbitrary function to check whether the peaks are placed correctly
% hold on
% plot(EList/(hbar*omegaC),sin(pi*EList/(hbar*omegaC)).^2*.05.*exp(-(EList/(hbar*omegaC)-30).^2/150))
% hold off

else
    figure;
    plot(tt,abs(AutoCorrWF))
    title('Wavefunction autocorrelation')
xlabel('t')
ylabel('|\langle\psi(0)|\psi(t)\rangle|')
print('AutoCorrWF','-dpng')


figure;
EList=-6.0:.005:6.0;
EList=EList*Vrms;
Spectrum=zeros(1,numel(EList));
for idx=1:numel(EList)
    EE=EList(idx);
Spectrum(idx)=trapz(tt,AutoCorrWF.*exp(1i*EE*tt/hbar))/(NTimeStep*Dt);
end
plot(EList/Vrms,abs(Spectrum))
title('Spectrum')
xlabel('$E/E_{unit}$','Interpreter','latex')
ylabel('Spectrum')
hold on
plot(EList/Vrms,.35*exp(-EList.^2/(2*(.5*Vrms)^2)))
hold off
% print('SpectrumWF','-dpng')
end