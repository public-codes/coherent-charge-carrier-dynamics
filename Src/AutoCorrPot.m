%% Spatial autocorrelation
% Wiener-Khinchin theorem to get numerical autocorrelation
AutoCorrV=ifft2(abs(fft2(V)).^2);
% AutoCorrV=fft2(abs(ifft2(V)).^2); %this also works, but has errorneous imaginary
% part
AutoCorrV=real(AutoCorrV);
AutoCorrV=AutoCorrV/AutoCorrV(1,1);
figure;

% Evaluating analytical expression for the autocorrelation
if choosePotential==1
AutocorrIntegrand=@(q,deltar) Ed*Ed.*q/(2*pi)/(2*pi)*2*hbar.*q/(rho_m*vs)*pi.*besselj(0,q*deltar)./(exp(hbar*vs.*q/kB/T)-1);
ACFN=arrayfun(@(deltar) integral(@(q) AutocorrIntegrand(q,deltar),0,qD),x);
ACFN=ACFN/integral(@(q) AutocorrIntegrand(q,0),0,qD);
end
if choosePotential==2
    ACFN=besselj(0,qphonon*x);
%     ACFN=A^2/2*besselj(0,qphonon*x);
end

% % plotting 2d landscape
% plot3(X,Y,AutoCorrV)

% % % plotting 1d slice
% numerically
plot(x,circshift(AutoCorrV(1,:),Ny/2),'LineWidth',2)

% analytically
hold on
plot(x,ACFN,'LineWidth',2)
%plot(x,cos(qphonon*x).^2)
% plot(x,besselj(0,qphonon*x))
if choosePotential==1
%     xline(2*pi/qD,'--','2\pi/q_D','LabelVerticalAlignment','bottom','FontSize',FontSizeAxes)
if qeff==qD
xline(2*pi/qD,'--','2\pi/q_D','LabelVerticalAlignment','middle','FontSize',FontSizeAxes)
xline(5.4/qD,'--','5.4/q_D','LabelVerticalAlignment','middle','LabelHorizontalAlignment','left','FontSize',FontSizeAxes)    
xline(8.5/qD,'--','8.5/q_D','LabelVerticalAlignment','middle','FontSize',FontSizeAxes)
% plot([0 5.4/qD],[-.05 -.05],'LineWidth',3)
% plot([0 8.5/qD],[-.1 -.1],'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
xlim([0 1]*20/qD)
else
    xline(2*pi/qeff,'--','2\pi/5q_B','LabelVerticalAlignment','middle','FontSize',FontSizeAxes)
    xlim([0 1]*20/qeff)
end
    
%     annotation('textarrow',[.284 .284], [.6 .23],'String','3.8317/q','FontSize',FontSizeAxes)
%     annotation('textarrow',[.46 .41], [.78 .58],'String','7.0156/q','FontSize',FontSizeAxes)
end
if choosePotential==2
    xline(2*pi/qphonon,'--','2\pi/q','LabelVerticalAlignment','bottom','FontSize',FontSizeAxes)
    xlim([0 1]*20/qphonon)
    annotation('textarrow',[.284 .284], [.6 .23],'String','3.8317/q','FontSize',FontSizeAxes)
    annotation('textarrow',[.46 .41], [.78 .58],'String','7.0156/q','FontSize',FontSizeAxes)
end

hold off
set(gca,'FontSize',FontSizeOverall)
% title('Autocorrelation function of potential','FontSize',FontSizeAxes)
title(['T/T_D=',num2str(T/TDebye)],'FontSize',FontSizeAxes)
xlabel('\deltar (m)','FontSize',FontSizeAxes)
ylabel('C(\deltar,0)/C(0,0)','FontSize',FontSizeAxes)
legend('Numerical','Analytical','FontSize',FontSizeAxes)




% figure;
% plot3(X,Y,AutoCorrV-circshift(autocorr,[1 1])/autocorr(end,end))
% 
% 
% return;


%% Temporal autocorrelation
figure;
% hou=zeros(1,1,NTimeStep+1);
% for idx1=1:Nx
%     for idx2=1:25:Ny
%         hou=hou+ifft(abs(fft(VHist(idx1,idx2,:),[],3)).^2,[],3);
%     end
% end
% 
% 
% % hou=zeros(1,1,NTimeStep+1);
% % for idx1=1:NGridPoints
% %     hou=hou+ifft(abs(fft(VHist(idx1,idx1,:),[],3)).^2,[],3);
% % end
% 
% 
% % Wiener-Khinchin theorem to get numerical autocorrelation
% % TemporalAutoCorrV=ifft(abs(fft(VHist(64,64,:))).^2);
% % TemporalAutoCorrV=ifftn(abs(fftn(VHist)).^2);
% TemporalAutoCorrV=hou;
% % AutoCorrV=fft(abs(ifft(VHist(1,1,:))).^2); %this also works, but has errorneous imaginary
% % part
% TemporalAutoCorrV=real(TemporalAutoCorrV);
% TemporalAutoCorrV=TemporalAutoCorrV/TemporalAutoCorrV(1,1);
% 
% 
% 
% 
% 
% Evaluating analytical expression for the autocorrelation
if choosePotential==1
AutocorrIntegrand=@(q,deltat) Ed^2.*q/(2*pi)*2*hbar.*q/(rho_m*vs).*cos(vs*q*deltat)/2./(exp(hbar*vs.*q/kB/T)-1);
% AutocorrIntegrand=@(q,deltat) Ed*Ed.*q/(2*pi)/(2*pi)*2*hbar.*q/(rho_m*vs)*pi.*besselj(0,q*vs*100*deltat).*cos(vs*q*deltat)./(exp(hbar*vs.*q/kB/T)-1);
ACFN=arrayfun(@(deltat) integral(@(q) AutocorrIntegrand(q,deltat),0,qD),tt);
ACFN=ACFN/integral(@(q) AutocorrIntegrand(q,0),0,qD);
end
if choosePotential==2
    ACFN=besselj(0,qphonon*x);
%     ACFN=A^2/2*besselj(0,qphonon*x);
end

% % % plotting 2d landscape
% % plot3(X,Y,AutoCorrV)
% 
% % % % plotting 1d slice
% % numerically
% plot(tt,TemporalAutoCorrV(:),'LineWidth',2,'DisplayName','Numerical')

% analytically

plot(tt,ACFN,'LineWidth',2,'DisplayName','Analytical')
hold on
%plot(x,cos(qphonon*x).^2)
% plot(x,besselj(0,qphonon*x))
if choosePotential==1
%     xline(2*pi/qD,'--','2\pi/q_D','LabelVerticalAlignment','bottom','FontSize',FontSizeAxes)
if qeff==qD
xline(2*pi/vs/qD,'--','2\pi/\omega_D','LabelVerticalAlignment','top','FontSize',FontSizeAxes)
xline(4.2/vs/qD,'--','4.2/\omega_D','LabelVerticalAlignment','top','FontSize',FontSizeAxes)
xline(7.6/vs/qD,'--','7.6/\omega_D','LabelVerticalAlignment','top','FontSize',FontSizeAxes)

% xline(5.4/qD,'--','5.4/q_D','LabelVerticalAlignment','bottom','FontSize',FontSizeAxes)    
% xline(8.5/qD,'--','8.5/q_D','LabelVerticalAlignment','bottom','FontSize',FontSizeAxes)
xlim([0 1]*20/vs/qD)

else
    xline(2*pi/vs/5/qB,'--','2\pi/5\omega_B','LabelVerticalAlignment','top','FontSize',FontSizeAxes)
    xlim([0 1]*20/vs/5/qB)
end
%     annotation('textarrow',[.284 .284], [.6 .23],'String','3.8317/q','FontSize',FontSizeAxes)
%     annotation('textarrow',[.46 .41], [.78 .58],'String','7.0156/q','FontSize',FontSizeAxes)
end
if choosePotential==2
    xline(2*pi/qphonon,'--','2\pi/q','LabelVerticalAlignment','bottom','FontSize',FontSizeAxes)
    xlim([0 1]*20/qphonon)
    annotation('textarrow',[.284 .284], [.6 .23],'String','3.8317/q','FontSize',FontSizeAxes)
    annotation('textarrow',[.46 .41], [.78 .58],'String','7.0156/q','FontSize',FontSizeAxes)
end

hold off
set(gca,'FontSize',FontSizeOverall)
% title('Autocorrelation function of potential','FontSize',FontSizeAxes)
title(['T/T_D=',num2str(T/TDebye)],'FontSize',FontSizeAxes)
xlabel('\deltat (s)','FontSize',FontSizeAxes)
ylabel('C(0,\deltat)/C(0,0)','FontSize',FontSizeAxes)
% legend('FontSize',FontSizeAxes)




% figure;
% plot3(X,Y,AutoCorrV-circshift(autocorr,[1 1])/autocorr(end,end))


return;




%% Spatio-temporal autocorrelation
% tSavingPeriod=0:50:NTimeStep;
% time=tSavingPeriod*Dt;
% 
% [XX,TT]=meshgrid(x,time);
% figure;
% 
% 
% AutocorrIntegrand=@(q,deltar,deltat) Ed*Ed.*q/(2*pi)/(2*pi)*2*hbar.*q/(rho_m*vs)*pi.*besselj(0,q*deltar).*cos(vs*q*deltat)./(exp(hbar*vs.*q/kB/T)-1);
% ACFN=arrayfun(@(deltar,deltat) integral(@(q) AutocorrIntegrand(q,deltar,deltat),0,qD),XX,TT);
% ACFN=ACFN/integral(@(q) AutocorrIntegrand(q,0,0),0,qD);
% 
% 
% % % plotting 2d landscape
% % plot3(X,Y,AutoCorrV)
% 
% % % % plotting 1d slice
% % numerically
% % plot(x,circshift(AutoCorrV(1,:),Ny/2),'LineWidth',2)
% surf(XX,TT,ACFN,'EdgeColor','none')
% hold on
% surf(XX,TT,exp(-((XX-vs*TT)/1e-10).^2),'EdgeColor','none')
% hold off
% colorbar
