
if dimension==2
    %% Free particle propagation analytic solution
    % %         timet=Dt*TimeStep;
    % %         xt=x0+(hbar*kF/m)*timet;
    % %         yt=y0;
    % %         Axt=Ax0/(1+2*Ax0*timet/m);
    % %         Ayt=Ay0/(1+2*Ay0*timet/m);
    % %         sxt=sx0+px0^2/(2*m)*timet+1j*hbar/2*log(1+2*Ax0*timet/m);
    % %         syt=sy0+py0^2/(2*m)*timet+1j*hbar/2*log(1+2*Ay0*timet/m);
    % %         st=sxt+syt;
    % %         AnalyticPsit=exp(1j/hbar*(Axt*(X-xt).^2+Ayt*(Y-yt).^2+hbar*kF*(X-xt)+0*(Y-yt)+st));



    %%

    AutoCorrWF(TimeStep+1)=Dx*Dy*sum(conj(psi0).*psi,'all');
    %         SpectrumSummand=SpectrumSummand+Dt*exp(1i*EList*Dt*TimeStep/hbar)*AutoCorrWF(TimeStep+1);
    % % % % % % Checking normalization of spectrum
    % % % % CheckNorm(TimeStep+1)=trapz(EList,SpectrumSummand/(pi*hbar));
    % % % % plot(abs(CheckNorm(1:500)))
    %


    %% % % calculating probability current J
    % % [GradXpsi,GradYpsi]=gradient(psi,Dx,Dy);
    % % vXpsi=(-1i*hbar*GradXpsi+e*Ax.*psi)/m;
    % % vYpsi=(-1i*hbar*GradYpsi+e*Ay.*psi)/m;
    % % % Jx=1/2*(conj(psi).*vXpsi+conj(vXpsi).*psi);
    % % % Jy=1/2*(conj(psi).*vYpsi+conj(vYpsi).*psi);
    % % Jx=real(conj(psi).*vXpsi);
    % % Jy=real(conj(psi).*vYpsi);


    %% Calculating various quantities every SavingPeriodAvgK
    if mod(TimeStep,SavingPeriodAvgK)==0
        psik=fft2(psi);
        psik0=fft2(psi0);
        PbDistRspace=abs(psi/sqrt(Dx*Dy*norm(psi(:))^2)).^2;
        PbDistKspace=abs(psik/sqrt(Dkx*Dky*norm(psik(:))^2)).^2;


        psik0norm=psik0/sqrt(Dkx*Dky*norm(psik0(:))^2);

        PbX=Dky*sum(PbDistRspace,1);
        avgKx(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*K1BZ(Kx,Nx,Dkx),'all')-Dx*Dy*sum(PbDistRspace.*Y,'all')*e*B/hbar;


        avgKy(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*K1BZ(Ky,Ny,Dky),'all');
        %     MSDK(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*(K1BZ(Kx,Nx,Dkx).^2+K1BZ(Ky,Ny,Dky).^2),'all');
        avgX(floor(TimeStep/SavingPeriodAvgK)+1)=Dx*Dy*sum(PbDistRspace.*X,'all');
        avgY(floor(TimeStep/SavingPeriodAvgK)+1)=Dx*Dy*sum(PbDistRspace.*Y,'all');
        %     NormTracker(floor(TimeStep/SavingPeriodAvgK)+1)=Dx*Dy*norm(psi(:))^2;

        if ScattPopAnal==1
            %         ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(((Kx-kF*cos(2*thetaB(1))).^2+(Ky-kF*sin(2*thetaB(1))).^2<=(.3*kF)^2).*PbDistKspace,'all')*Dkx*Dky;
            ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(((Kx-kF).^2+Ky.^2>(.2*kF)^2).*PbDistKspace,'all')*Dkx*Dky;

            %         ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(2*thetaB(1)-.3<=KAngleGrid & KAngleGrid<2*thetaB(1)+.3).*PbDistKspace,'all')*Dkx*Dky;
            %         ScatteredPopulation2(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(-2*thetaB(1)-.3<=KAngleGrid & KAngleGrid<-2*thetaB(1)+.3).*PbDistKspace,'all')*Dkx*Dky;
            %         ScatteredPopulation3(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(-.3<=KAngleGrid & KAngleGrid<.3).*PbDistKspace,'all')*Dkx*Dky;
            %             ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(KAngleBins((NBins+1)/2+1)<=KAngleGrid & KAngleGrid<KAngleBins((NBins+1)/2+1)+1.).*PbDistKspace,'all')*Dkx*Dky;
            %             ScatteredPopulation2(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(KAngleBins((NBins+1)/2)-1.<=KAngleGrid & KAngleGrid<KAngleBins((NBins+1)/2)).*PbDistKspace,'all')*Dkx*Dky;
            %             ScatteredPopulation3(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(KAngleBins((NBins+1)/2)<=KAngleGrid & KAngleGrid<KAngleBins((NBins+1)/2+1)).*PbDistKspace,'all')*Dkx*Dky;
        end
        %

    end





    %%         Plot every SavingPeriod
    if mod(TimeStep,SavingPeriod)==0 && plotMRK==1

        if UseGPU==1
            V=gather(V);
            V=real(V);
            psi=gather(psi);
        end


        tiledlayout(1,3);

        nexttile;
        PlotWFReal;

                                nexttile;
                                PlotWFReciprocal;






                        nexttile;
                        plot(time,avgKx,'DisplayName','Numerical')
                        hold on
                        plot(time,avgKx(1)*exp(-time/tauQPT),'DisplayName','PT')
                        hold off
                        xlim([0 max([TimeStep,1])*Dt])
                        xlabel('t')
                        ylabel('\langle k_x \rangle')
                        legend()

        %
        %
        % %                 data fitting for exponential decay
        %                 %                 FitMin=1;
        %                 %                 FitMax=min([floor(TimeStep/SavingPeriodAvgK) round(tauQPT/Dt/SavingPeriodAvgK)]);
        %                 %                 [linearfitk,ErrorStruc]=polyfit(time(FitMin:FitMax),log(avgKx(FitMin:FitMax)),1);%fitting upto FitMax-th savedtimestep
        %                 %                 tauk=-1/linearfitk(1);
        %                 %                 intercept=linearfitk(2);
        %                 %                 expfnk=avgKx(1)*exp(-time/tauk);
        %                 %
        %                 %
        %                 %                 hold on
        %                 %                 plot(time,expfnk)
        %                 %                 hold off








        %                         nexttile;
        %                         if UseGPU==1
        %                             psiE0=gather(psiE0);
        %                         end
        %                 PlotImage2D(X,Y,real(psiE0),'WF')
        % %                 PlotImage2D(X,Y,abs(psiE0).^2,'WF')
        %                                                 title(['Eigenstate at E=',num2str(E0/Vrms,'%.3f'),'V_{rms}'])


        %


        %                                 nexttile;
        %                                 plot(tt,abs(AutoCorrWF))
        %                                 title('Wavefunction autocorrelation')
        %                                 xlabel('t')
        %                                 ylabel('|\langle\psi(0)|\psi(t)\rangle|')
        %                                 xlim([0 max([TimeStep,1])*Dt])
        %                                 %         print('AutoCorrWF','-dpng')


        %         nexttile;
        % % % %         Naive way
        % %         for idx=1:numel(EList)
        % %             EE=EList(idx);
        % %             Spectrum(idx)=trapz(tt,AutoCorrWF.*exp(1i*EE*tt/hbar));
        % %         end
        %         plot(EList/Vrms,abs(SpectrumSummand))
        %
        %         title('Spectrum')
        %         xlabel('$E/V_{rms}$','Interpreter','latex')
        %         ylabel('Spectrum')
        %         hold on
        % %         plot(EList/Vrms,abs(Spectrum))
        % %         plot(EList/Vrms,.35*exp(-EList.^2/(2*(.5*Vrms)^2)))
        %         hold off
        %         % print('SpectrumWF','-dpng')
        %



        % nexttile;
        % %                 plot(time,avgTkinx+avgV-avgTkinx(1)-avgV(1),'DisplayName','\langleH\rangle-\langleH\rangle_0')
        %                 plot(time,avgTkinx+avgV,'DisplayName','\langleH\rangle')
        %                 xlim([0 max([TimeStep,1])*Dt])
        %                 xlabel('t')
        %                 ylabel('Energy')
        %             legend()

        MRK(TimeStep/SavingPeriod+1)=getframe(gcf);
        %             tLoopEnd=cputime-tLoopStart-tLoopEnd;
        disp(['Loop ',num2str(TimeStep),'/',num2str(NTimeStep),': ',num2str(toc),' s'])
        tic

        if UseGPU==1
            V=gpuArray(V);
            psi=gpuArray(psi);
        end

        clear psik PbDistKspace PbDistRspace;

    end

end