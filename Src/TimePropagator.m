% psi=expVimag.*psi;
if nonlinSE==1
    if order==1
        expV=exp(-1i*(V+gN*abs(psi).^2)*Dt/hbar);
    end
    if order==2
        expVover2=exp(-1i*(V+gN*abs(psi).^2)/2*Dt/hbar);
    end
end

%% script version
if order==1
    expV=exp(-1i*V*Dt/hbar);
    psi=ifft2(expT.*fft2(expV.*psi));
end
if order==2
    expVover2=exp(-1i*V/2*Dt/hbar);
    if B == 0
        psi=expVover2.*ifft2(expT.*fft2(expVover2.*psi));
    end
end


