%%

if UseGPU==1
    V=gather(V);
    V=real(V);
end
figure('Position',[000 000 600 600])
if dimension==1
    plot(x,V)
end

if dimension==2
%     ax111=axes();
%     contour(X,Y,V,'Fill','on')

PlotImage2D(X,Y,V,'Pot')


% title(['Potential',newline,'(real space)'],'FontSize',FontSizeAxes)
% xlabel('x/m','FontSize',FontSizeAxes)
% ylabel('y/m','FontSize',FontSizeAxes)
%         hold on
%         %     to indicate wave packet upto 2*sigma
%         %     ax222=axes('Position',ax111.Position);
%         % for plotting ellipse
%         ang=0:pi/50:2*pi;
%         xc=x0+3*sigmax*cos(ang);%circle coordinates
%         yc=y0+3*sigmay*sin(ang);
%
%         plot(xc,yc,':k')
%         % pbaspect([XSize YSize 1])
%         % set(ax222,'XLim',ax111.XLim,'YLim',ax111.YLim,'Visible','off');
%         hold off

if choosePotential==1
hold on
% plot([X(1,floor(Nx*.8)) X(1,floor(Nx*.8))+5.5/qD],[Y(floor(Nx*.1),1) Y(floor(Nx*.1),1)],'LineWidth',5)
% plot([X(1,floor(Nx*.8)) X(1,floor(Nx*.8))+8.5/qD],[Y(floor(Nx*.09),1) Y(floor(Nx*.09),1)],'LineWidth',5,'Color',[0.8500 0.3250 0.0980])
% plot([X(1,floor(Nx*.8)) X(1,floor(Nx*.8))+2*pi/5/qB],[Y(floor(Nx*.09),1) Y(floor(Nx*.09),1)],'LineWidth',5,'Color',[0.8500 0.3250 0.0980])
% xlim([X(1,floor(Nx*.65)) X(1,floor(Nx*.85))])
% ylim([Y(floor(Nx*.05),1) Y(floor(Nx*.25),1)])
hold off
% xlim([X(1,floor(Nx*.25)) X(1,floor(Nx*.75))])
% ylim([Y(floor(Nx*.25),1) Y(floor(Nx*.75),1)])
% xlim([X(1,floor(Nx*.375)) X(1,floor(Nx*.625))])
% ylim([Y(floor(Nx*.375),1) Y(floor(Nx*.625),1)])
end
if choosePotential==2
    hold on
    %plot([X(1,floor(Nx*.8)) X(1,floor(Nx*.8))+3.8317/qphonon],[Y(floor(Nx*.1),1) Y(floor(Nx*.1),1)],'LineWidth',5)
    %plot([X(1,floor(Nx*.8)) X(1,floor(Nx*.8))+7.0156/qphonon],[Y(floor(Nx*.09),1) Y(floor(Nx*.09),1)],'LineWidth',5)
    hold off
end
end



%%
% Plotting Fourier transform of potential
% figure;
figure('Position',[600 000 600 600])
if dimension==1
    plot(Kx,abs(fft(V)))
end
if dimension==2

    PlotImage2D(Kx,Ky,abs(fft2(V)).^2,'PbK')
    


title(['Potential',newline,'(reciprocal space)'],'FontSize',FontSizeAxes)
xlabel('k_x/m^{-1}','FontSize',FontSizeAxes)
ylabel('k_y/m^{-1}','FontSize',FontSizeAxes)

if choosePotential==1
hold on
rectangle('Position',[-1 -1 2 2]*qx(end),'EdgeColor','r')
rectangle('Position',[-1 -1 2 2]*min([5*kB*T/hbar/vs,qD]),'EdgeColor','g')
hold off
end
end

if UseGPU==1
    V=gpuArray(V);
end