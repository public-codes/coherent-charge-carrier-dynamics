%     V=zeros(Ny,Nx);
if loadPotential==1
    load('MyPotential','V')
else

    if choosePotential==1
        if dimension==1
            if UseGPU==1
                V=ifft(gpuArray(dpotTDFFT1D(Kx,TimeStep*Dt,XSize,vs,Nx,Ed,hbar,rho_m,kB,T,phase,qD,Vol,TDtuning)));
            else
                V=ifft(dpotTDFFT1D(kx,TimeStep*Dt,XSize,vs,Nx,Ed,hbar,rho_m,kB,T,phase,qD,Vol,TDtuning));
            end

%                         V=dpotAnother1D(x,vs,Kx,Ed,hbar,rho_m,kB,T,phase,qD,Vol,Nx);


        end

        if dimension==2
            % % % % % %             %     V=dpot(X,Y,vs,nqx,nqy,qx,qy,Ed,hbar,rho_m,kB,T,phase,qD,Vol);
            %             V=dpotTD(X,Y,TimeStep*Dt,vs,nqx,nqy,qx,qy,Ed,hbar,rho_m,kB,T,phase,qD,Vol,TDtuning);

            %     qabs=sqrt(Kx.^2+Ky.^2);


                V=dpotAnother(X,Y,vs,Kx,Ky,Ed,hbar,rho_m,kB,T,phase,qD,Vol,Nx);



        end
        lambda=2*pi/kF;
    end



end


Vrms=sqrt(mean(V(:).^2));%This is an estimate of the rms of the potential. In our case, the average value of the potential is zero, so rms value will be std(V(:)).
%A/sqrt(2);

if UseGPU==1
Vrms=real(gather(Vrms));
end

% save('MyPotential','V')

%     V=V.*ctsconnect(X,-7*L*HbarRatio,-6.5*L*HbarRatio);
%          V=V.*sigmoid(X,-0.3*L*HbarRatio,0.1*L*HbarRatio);
%     V=V.*takecircle(X,Y,5*L);

% % FlatHeight=-.3*Vrms;
% % FlatHeight=.1*Vrms;
% FlatHeight=-0.*Vrms;
% Vdisk=takesmoothcircle(X,Y,.15*L,.01*L);
%     V=V.*(1-Vdisk)+FlatHeight*Vdisk;


%     V=V.*(0*(Y<0)+1*(Y>0));


%         save(['ModelPot'],'V')

% % adding electric potential
% Voltage=1;
% Ex=Voltage/XSize;
% VE=-e*Ex*X;
% V=V+VE;

% % % adding imaginary potential for absorbing boundaries
% Vimag=zeros(Ny,Nx);
% widthimag=.02*XSize;
% % Vimag=sigmoid(X,XSize*.4,widthimag)+sigmoid(-X,XSize*.4,widthimag)+sigmoid(Y,YSize*.4,widthimag)+sigmoid(-Y,YSize*.4,widthimag);
% % the above choice unnecessarily emphasizes the corner
%
% % Matrix of shortest distance to the boundaries
% ShortestDist=min(min(min(XSize/2+X,XSize/2-X),YSize/2+Y),YSize/2-Y);
% % % % Vimag=(ShortestDist<widthimag)*10*e; % Hard wall potential. Not good for absorption due to impedance mismatch
% % Vimag=sigmoid(-(ShortestDist-XSize*.02),0,widthimag)*5*e;
% % imagesc([X(1) X(end)],[Y(1) Y(end)],Vimag);
% % load('RWBcmap.mat')
% % % colormap(flipud(gray))
% % pbaspect([1 1 1])
% % colormap(gray)
% % set(gca,'YDir','normal')
%



function val=ctsconnect(x,x1,x2)
val=0*(x<x1)+(x-x1)/(x2-x1).*(x1<x & x<x2)+1*(x>x2);
end

function val=sigmoid(x,x0,width)
val=1./(1+exp(-(x-x0)/width));
end

function val=takecircle(x,y,radius)
val=0*(sqrt(x.^2+y.^2)>radius)+1*(sqrt(x.^2+y.^2)<radius);
end

function val=takesmoothcircle(x,y,radius,width)
val=1./(1+exp((sqrt(x.^2+y.^2)-radius)/width));
end