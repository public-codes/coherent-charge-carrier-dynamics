%data fitting for px_avg
FitMin=1;
FitMax=min([floor(NTimeStep/SavingPeriodAvgK) round(tauQPT/Dt/SavingPeriodAvgK)]);
[linearfitk,ErrorStruc]=polyfit(time(FitMin:FitMax),log(avgKx(FitMin:FitMax)),1);%fitting upto FitMax-th savedtimestep
tauk=-1/linearfitk(1);
intercept=linearfitk(2);
expfnk=avgKx(1)*exp(-time/tauk);
% expfnk=exp(-time/tauk+intercept);
expfnPT=avgKx(1)*exp(-time/tauQPT);
% expfnPT=exp(-time/tauQPT+intercept);

%kx_avg plot
figure('Position',[0 0 1300 650])
subplot(1,2,1)
plot(time,avgKx)
title('x component of average wavevector')
hold on
plot(time,expfnk)
if choosePotential==1
plot(time,expfnPT)
end
hold off
legend('simulation','fitting','QuantumPT')

%kx_avg log plot
subplot(1,2,2)
semilogy(time,avgKx)
title('x component of average wavevector(log plot)')
hold on
semilogy(time,expfnk)
if choosePotential==1
semilogy(time,expfnPT)
end
hold off
legend('simulation','fitting','QuantumPT')

print([FigureDir DataLabel '_AvgKPlots'],'-dpng')