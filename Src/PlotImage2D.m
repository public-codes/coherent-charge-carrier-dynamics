function PlotImage2D(X,Y,Z,cmapIdx)
Size=size(Z);
Ny=Size(1);
Nx=Size(2);


if strcmp(cmapIdx,'PbK')
    X=circshift(X,[Ny/2 Nx/2]);
    Y=circshift(Y,[Ny/2 Nx/2]);
    Z=circshift(Z,[Ny/2 Nx/2]);
end

imagesc(gca,[X(1) X(end)],[Y(1) Y(end)],real(Z));


if strcmp(cmapIdx,'WF')
    cmap;
    colormap(gca,uu)
    cmapRange=[-1 1]*.5;
    xlim([X(1,1) X(1,end)])
    ylim([Y(1,1) Y(end,1)])
else
    if strcmp(cmapIdx,'Pot')
        colormap(gca,gray)
        % colormap(flipud(gray))
        cmapRange=[-1 1]*1.;
    else
        if strcmp(cmapIdx,'PbK')
            load('MyColormap.mat')
            colormap(gca,mymap)
            cmapRange=[0 1]*1.;
        end
    end
end



if max(abs(Z),[],'all')~=0
    caxis(gca,cmapRange*max(abs(Z),[],'all'))
end

if strcmp(cmapIdx,'PbK')
    pbaspect([1 1 1])
else
    pbaspect([Nx Ny 1])
end

set(gca,'YDir','normal')


% % % colorbar
% % % set(gca,'FontSize',FontSizeOverall)
% set(gca,'XTickLabel',[])
% set(gca,'XTick',[])
% set(gca,'YTickLabel',[])
% set(gca,'YTick',[])

end