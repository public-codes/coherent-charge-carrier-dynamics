%% Plot resistivity (or more accurately, total scattering rate)

%% Determining font size depending on OS
if ismac==1
    FontSizeOverall=18;
    FontSizeAxes=24;
else
    if isunix==1
        FontSizeOverall=18;
        FontSizeAxes=18;
    else
        FontSizeOverall=14;
        FontSizeAxes=18;
    end
end


%% 2D (phase diagram)
% % % % From perturbation theory
% % UseRelativeScale=1;
% % TRatio=1;
% % kFToqDover2=1;
% % InputMock2D;
% % 
% % hbarRatiolist=.01:.02:2.;%for lambda change
% % kFToqDover2List=.5:.02:3.0;
% % TRatioList=0.01:.02:2.0;
% % AndLocList=zeros(1,numel(TRatioList));
% % InvTauPT=zeros(numel(TRatioList),numel(kFToqDover2List));
% % arbitRatio=0.5;
% % hbar0=hbar;
% % 
% % InvTauMIR=vF*qD/pi;
% % % InvTauUniversal=kB*T/hbar;
% % 
% % % for ind=1:length(hbarRatiolist)
% % for ind=1:length(kFToqDover2List)
% % for ind2=1:length(TRatioList)
% % %     lambdaratio=hbarRatiolist(ind);
% %     lambdaratio=1/kFToqDover2List(ind);
% %     Tratio=TRatioList(ind2);
% % 
% %     kF=qD/2/lambdaratio;
% %     T=TDebye*Tratio;
% % %     hbar=hbar*lambdaratio;
% %     qmax=min(2*kF,qD);
% %     InverseTauQPTIntegrand=@(q) m/(2*pi*hbar^3*kF^3).*q.^2./sqrt(1-(q/(2*kF)).^2)*Ed^2*hbar.*q/(rho_m*vs)./(exp(hbar*vs.*q/kB/T)-1);
% %     InvTauPT(ind2,ind)=integral(InverseTauQPTIntegrand,0,qmax);
% % 
% %     VsqIntegrand=@(q) Ed*Ed*2*pi/(2*pi)/(2*pi)*2*hbar.*q.^2/(rho_m*vs)*1/2./(exp(hbar*vs.*q/kB/T)-1);
% %     Vsq=integral(VsqIntegrand,0,qD);%This is an analytic estimate of the mean square of the potential, mean(V(:).^2).
% %     Vrms=sqrt(Vsq);%This is an analytic estimate of the rms of the potential. In our case, the average value of the potential is zero, so rms value will be std(V(:)).
% %     AndLocList(ind2)=sqrt(2*m*Vrms/arbitRatio)/hbar*2/qD;
% %     
% %     kF=kF*lambdaratio;
% %     T=T/Tratio;
% % %     hbar=hbar/lambdaratio;
% % end
% % end
% % 
% % figure('Position',[0 0 1300 650])
% % tiledlayout(1,2)
% % 
% % nexttile;
% % % imagesc(qD./(2*kF./hbarRatiolist),TRatioList*T/TDebye,InvTauPT)
% % imagesc(2*kF/qD.*kFToqDover2List,TRatioList*T/TDebye,InvTauPT)
% % colorbar
% % set(gca,'YDir','normal','FontSize',FontSizeOverall)
% % % xlabel('q_D/2k_F')
% % xlabel('2k_F/q_D','FontSize',FontSizeAxes)
% % % xlabel('hbar/hbar0')
% % ylabel('T/T_D','FontSize',FontSizeAxes)
% % % title('hbar study, potential fixed about hbar change')
% % % title('hbar study')
% % % title(['\Gamma_{MIR}=',num2str(InvTauMIR,'%e'),newline,'UniversalScatteringFarStronger'])
% % title('1/\tau (Perturbation)','FontSize',FontSizeAxes)
% % hold on
% % plot(AndLocList,TRatioList,'r')
% % % plot(kFToqDover2List,min(kFToqDover2List,ones(1,numel(kFToqDover2List))),'g')
% % plot(kFToqDover2List,min(.1*kFToqDover2List,.1*ones(1,numel(kFToqDover2List))),'c')
% % hold off
% % legend(['V_{rms}/E_F=',num2str(arbitRatio)],'0.1T_{BG} or 0.1T_D','FontSize',FontSizeAxes)
% % text(1.1,.05,'1/\tau~T^4','Color','white','FontSize',FontSizeAxes)
% % text(1.1,.25,'1/\tau~T','Color','white','FontSize',FontSizeAxes)
% % text(.5,1.6,['Anderson',newline,'localization'],'Color','white','FontSize',FontSizeAxes)
% % 
% % % nexttile;
% % % % [XX,YY]=meshgrid(hbarRatiolist*T/TDebye,TRatioList);
% % % % contour(gca,XX,YY,InvTauPT,10)
% % % [XX,YY]=meshgrid(1.*kFToqDover2List*T/TDebye,TRatioList);
% % % contour(gca,XX,YY,InvTauPT,10)
% 
% 
% 
% 
% % % From numerics
TRatioList=.00:.05:2.0;
kFToqDover2List=.5:.05:2.00;
WindowSize=1;


% for idx1=1:numel(TRatioList)
%     for idx2=1:numel(kFToqDover2List)
%         UseRelativeScale=1;
%         TRatio=TRatioList(idx1);
%         kFToqDover2=kFToqDover2List(idx2);
%         
%         % call input parameters to know JobName.
%         InputMock2D;
%         
%         if isfile([FigureDir DataLabel '_SomeVars.mat'])
%         load([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT','tauzero','tau1st','Vrms','EF')
%         tau1st=[];
%         save([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT','tauzero','tau1st','Vrms','EF')
%         end
%     end
% end
% 
% return;

ScatteringRates=zeros(numel(TRatioList),numel(kFToqDover2List));
ScatteringRates2=zeros(numel(TRatioList),numel(kFToqDover2List));
ScatteringRates3=zeros(numel(TRatioList),numel(kFToqDover2List));


AndLocList=zeros(1,numel(TRatioList));
ValidPTList=zeros(1,numel(TRatioList));
ClList=zeros(1,numel(TRatioList));
arbitRatio=0.3;
arbitRatio2=.18;

for idx1=1:numel(TRatioList)
    for idx2=1:numel(kFToqDover2List)
        UseRelativeScale=1;
        TRatio=TRatioList(idx1);
        kFToqDover2=kFToqDover2List(idx2);
        
        % call input parameters to know JobName.
        InputMock2D;
        
        if isfile([FigureDir DataLabel '_SomeVars.mat'])
        load([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT','tauzero','tau1st','Vrms','EF')
        ScatteringRates(idx1,idx2)=1/tauk;
        
        if isempty(tauzero)==1
%             ScatteringRates(idx1,idx2)=1/tauk;
        else
%             ScatteringRates2(idx1,idx2)=1/min([tauk tauzero]);
            ScatteringRates2(idx1,idx2)=1/tauzero;
        end
        
        
        if isempty(tau1st)==1
%             ScatteringRates3(idx1,idx2)=1/tauk;
        else
%             ScatteringRates3(idx1,idx2)=1/min([tauk tauzero]);
            ScatteringRates3(idx1,idx2)=1/tau1st;
            clear tau1st;
        end
        end        
    end
    AndLocList(idx1)=sqrt(2*m*VrmsAnal/arbitRatio)/hbar*2/qD;
    ValidPTList(idx1)=sqrt(2*m*VrmsAnal/arbitRatio2)/hbar*2/qD;
    ClList(idx1)=min([3*kB*T/hbar/vs,qD])*1.5/qD;
end



% % figure('Position',[0 0 1300 650])
% % tiledlayout(1,2)
% 
% % nexttile;

% save('ScatRates','ScatteringRates','ScatteringRates2','ScatteringRates3')
% load('ScatRates')

figure;
% imagesc(kFToqDover2List,TRatioList,ScatteringRates)

% s=pcolor(kFToqDover2List,TRatioList,ScatteringRates*1e-15);
% s.FaceColor='interp';
% s.EdgeColor='none';

contourf(kFToqDover2List,TRatioList,ScatteringRates*1e-15,50,'LineColor','none');
colormap('jet')
colorbar
set(gca,'YDir','normal','FontSize',FontSizeOverall)
% xlabel('q_D/2k_F')
xlabel('2k_F/q_D','FontSize',FontSizeAxes)
% xlabel('hbar/hbar0')
ylabel('T/T_D','FontSize',FontSizeAxes)
% title('hbar study, potential fixed about hbar change')
% title('hbar study')
% title(['\Gamma_{MIR}=',num2str(InvTauMIR,'%e'),newline,'UniversalScatteringFarStronger'])
% title('1/\tau (Numerical)','FontSize',FontSizeAxes)
title('1/\tau (fs^{-1})','FontSize',FontSizeAxes)
hold on
plot(AndLocList,TRatioList,'r--','DisplayName',['V_{rms}/E_F=',num2str(arbitRatio)],'LineWidth',1.5)
% plot(ValidPTList,TRatioList,'m--','DisplayName',['V_{rms}/E_F=',num2str(arbitRatio2)])
% plot(ClList,TRatioList,'g--','DisplayName',['q_B(T)=kF'])
% plot(kFToqDover2List,min(kFToqDover2List,ones(1,numel(kFToqDover2List))),'g')
plot(kFToqDover2List,min(.1*kFToqDover2List,.1*ones(1,numel(kFToqDover2List))),'w--','DisplayName','0.1T_{BG} or 0.1T_D','LineWidth',1.5)
hold off
% legend('FontSize',FontSizeAxes)
% text(2.1,.10,'1/\tau~T^4','Color','white','FontSize',FontSizeAxes,'FontName','Times','FontWeight','bold')
% text(2.1,.35,'1/\tau~T','Color','white','FontSize',FontSizeAxes,'FontName','Arial')
% text(.5,1.6,['Anderson',newline,'localization'],'Color','white','FontSize',FontSizeAxes)
text(.95,.45,['II'],'Color','white','FontSize',FontSizeAxes+2,'FontName','Nimbus Roman','FontWeight','bold')
% text(.95,1.0,['B'],'Color','white','FontSize',FontSizeAxes)
text(.75,1.9,['I'],'Color','white','FontSize',FontSizeAxes+2,'FontName','Nimbus Roman','FontWeight','bold')
text(1.8,1.0,['III'],'Color','white','FontSize',FontSizeAxes+2,'FontName','Nimbus Roman','FontWeight','bold')
% text(.5,1.25,['Nonexp. & PT less reliable'],'Color','white','FontSize',FontSizeAxes)

xlim([kFToqDover2List(1) Inf])

% nexttile;
% [XX,YY]=meshgrid(kFToqDover2List,TRatioList);
% contour(gca,XX,YY,ScatteringRates,10)

% print('phasedia','-dpng')


% figure;
% contourf(kFToqDover2List,TRatioList,ScatteringRates2*1e-15,50);
% colorbar
% 
% figure;
% contourf(kFToqDover2List,TRatioList,ScatteringRates3*1e-15,50);
% colorbar
% 
% figure;
% contourf(kFToqDover2List,TRatioList,ScatteringRates+ScatteringRates2,30);
% colorbar
% 
% figure;
% contourf(kFToqDover2List,TRatioList,ScatteringRates+ScatteringRates3,30);
% colorbar
% 
% figure;
% contourf(kFToqDover2List,TRatioList,max(ScatteringRates,ScatteringRates2),30);
% colorbar

%% 1D (temperature dependence-old)
% 
% UseRelativeScale=1;
% % % choose kF value
% kFToqDover2=2.8;
% 
% % % interested temperature range
% % TRatioList=.25:.25:1.5;
% % TRatioList=.01:.05:2;
% % TRatioList=.05:.05:2.0;
% % TRatioList=.1:.1:5.0;
% 
% NTemps=60;
% TRatioList=.05*(10/.05).^((0:NTemps)/NTemps);
% 
% ScatteringRates=zeros(1,numel(TRatioList));
% ScatteringRatesPT=zeros(1,numel(TRatioList));
% ScatteringRatesPTHighT=zeros(1,numel(TRatioList));
% ScatteringRatesPTHighTNaive=zeros(1,numel(TRatioList));
% ScatteringRatesPTLowT=zeros(1,numel(TRatioList));
% ScatteringRatesPTFock=zeros(1,numel(TRatioList));
% ScatteringRatesPTLowTInel=zeros(1,numel(TRatioList));
% 
% for idx1=1:numel(TRatioList)
%         TRatio=TRatioList(idx1);
%         
%         % call input parameters to know JobName.
%         InputMock2D;
%         
%         
% %         if isfile([FigureDir DataLabel '_SomeVars.mat'])
% %         load([FigureDir DataLabel '_SomeVars'])
%         ScatteringRates(idx1)=1/tauk;
%         ScatteringRatesPT(idx1)=InverseTauQPT;
%         ScatteringRatesPTFock(idx1)=InverseTauQPTFock;
% ScatteringRatesPTHighT(idx1)=InverseTauQPTHighT;
% ScatteringRatesPTHighTNaive(idx1)=InverseTauQPTHighTNaive;
% ScatteringRatesPTLowT(idx1)=InverseTauQPTLowT;
% ScatteringRatesPTLowTInel(idx1)=InverseTauQPTLowTInel;
% %         end
%         
% end
% 
% 
% %data fitting for ScatteringRates
% FitMin=find(TRatioList>3,1);
% linearfitGamma=polyfit(log(TRatioList(FitMin:end)),log(ScatteringRates(FitMin:end)),1);%fitting upto FitMax-th savedtimestep
% GammaSlope=linearfitGamma(1);
% GammaIntercept=linearfitGamma(2);
% linearfnGamma=exp(GammaSlope*log(TRatioList)+GammaIntercept);
% 
% linearfitGammaPT=polyfit(log(TRatioList(FitMin:end)),log(ScatteringRatesPT(FitMin:end)),1);%fitting upto FitMax-th savedtimestep
% GammaSlopePT=linearfitGammaPT(1);
% GammaInterceptPT=linearfitGammaPT(2);
% linearfnGammaPT=exp(GammaSlopePT*log(TRatioList)+GammaInterceptPT);
% 
% % figure('Position',[0 0 1300 650])
% % tiledlayout(1,2)
% % nexttile;
% % plot(TRatioList,ScatteringRates,'DisplayName','numerical','LineWidth',2)
% % hold on
% % plot(TRatioList,ScatteringRatesPT,'DisplayName','PT','LineWidth',2)
% % plot(TRatioList(round(numel(TRatioList)*.3):end),ScatteringRatesPTHighT(round(numel(TRatioList)*.3):end),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off') %,'HandleVisibility','off' is for removing legend %,'DisplayName','PTHighT'
% % plot(TRatioList(round(numel(TRatioList)*.3):end),ScatteringRatesPTHighTNaive(round(numel(TRatioList)*.3):end),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off') %,'HandleVisibility','off' is for removing legend %,'DisplayName','PTHighT'
% % plot(TRatioList(1:round(numel(TRatioList)*.2)),ScatteringRatesPTLowT(1:round(numel(TRatioList)*.2)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off') %,'DisplayName','PTLowT'
% % hold off
% % legend('Location','southeast')
% % xlabel('T/T_D')
% % ylabel('1/\tau')
% % xlim([min(TRatioList) max(TRatioList)])
% % ylim([-inf max(ScatteringRates)*1.1])
% % title(['2k_F/q_D=',num2str(kFToqDover2)])
% % 
% % nexttile
% figure('Position',[0 0 650 650])
% loglog(TRatioList,ScatteringRates*1e50,'DisplayName','Numerical','LineWidth',2,'HandleVisibility','off')
% 
% hold on
% loglog(TRatioList,ScatteringRatesPT,'DisplayName','PT','LineWidth',2)
% loglog(TRatioList,ScatteringRatesPTFock,'DisplayName','PTFock','LineWidth',2)
% % loglog(TRatioList(round(numel(TRatioList)*.3):end),ScatteringRatesPTHighT(round(numel(TRatioList)*.3):end),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off')%,'DisplayName','PTHighT'
% loglog(TRatioList(round(numel(TRatioList)*.6):end),ScatteringRatesPTHighTNaive(round(numel(TRatioList)*.6):end),'k--','LineWidth',1,'HandleVisibility','off')%,'DisplayName','PTHighT'
% loglog(TRatioList(1:round(numel(TRatioList)*.4)),ScatteringRatesPTLowT(1:round(numel(TRatioList)*.4)),'k--','LineWidth',1,'HandleVisibility','off')%,'DisplayName','PTLowT'
% loglog(TRatioList(1:round(numel(TRatioList)*.4)),ScatteringRatesPTLowTInel(1:round(numel(TRatioList)*.4)),'k--','LineWidth',1,'HandleVisibility','off')%,'DisplayName','PTLowT'
% % loglog(TRatioList(round(numel(TRatioList)*.02):end),linearfnGamma(round(numel(TRatioList)*.02):end),'--','Color',[0 0.4470 0.7410],'DisplayName',['numerical HighT fitting',newline,'slope=',num2str(GammaSlope)],'LineWidth',1)
% % loglog(TRatioList(round(numel(TRatioList)*.02):end),linearfnGammaPT(round(numel(TRatioList)*.02):end),'--','Color',[0.8500 0.3250 0.0980],'DisplayName',['PT HighT fitting',newline,'slope=',num2str(GammaSlopePT)],'LineWidth',1)
% hold off
% set(gca,'FontSize',FontSizeOverall)
% legend('Location','northwest','FontSize',FontSizeAxes)
% xlabel('T/T_D','FontSize',FontSizeAxes)
% ylabel('1/\tau (s^{-1})','FontSize',FontSizeAxes)
% title(['2k_F/q_D=',num2str(kFToqDover2)],'FontSize',FontSizeAxes)
% text(7e-2,5e10,'~T^4','Color','black','FontSize',FontSizeAxes)
% text(1.5e-1,2e10,'~T^4','Color','black','FontSize',FontSizeAxes)
% text(2e0,1.5e13,'~T','Color','black','FontSize',FontSizeAxes)
% xlim([min(TRatioList) max(TRatioList)])
% ylim([-inf max(ScatteringRatesPT)*1.1])


%% 1D (temperature dependence-new)



SweepParameter1List=343;
% SweepParameter1List=[14 90 170 301];
SweepParameter2List=70;


VrmsList=zeros(1,numel(SweepParameter1List));
ScatteringRates=zeros(1,numel(SweepParameter1List));
ScatteringRatesPT=zeros(1,numel(SweepParameter1List));
ScatteringRatesPTHighT=zeros(1,numel(SweepParameter1List));
ScatteringRatesPTHighTNaive=zeros(1,numel(SweepParameter1List));
ScatteringRatesPTLowT=zeros(1,numel(SweepParameter1List));
ScatteringRatesPTFock=zeros(1,numel(SweepParameter1List));
ScatteringRatesPTCoherent=zeros(1,numel(SweepParameter1List));
ScatteringRatesPTLowTInel=zeros(1,numel(SweepParameter1List));

for idx1=1:numel(SweepParameter1List)
    for idxx2=1:length(SweepParameter2List)
        SweepParameter1=SweepParameter1List(idx1);
        SweepParameter2=SweepParameter2List(idxx2);

        JobName=[num2str(SweepParameter1),'_',num2str(SweepParameter2),''];
        new_filename = ['jobfiles/','Input_',JobName,'.m'];

        % call input parameters to know JobName.
        run(new_filename);
        
        
%         if isfile([FigureDir DataLabel '_SomeVars.mat'])
%         load([FigureDir DataLabel '_SomeVars'])
%         ScatteringRates(idx1)=1/tauk;
VrmsList(idx1)=VrmsAnal;
        ScatteringRatesPT(idx1)=InverseTauQPT;
        ScatteringRatesPTFock(idx1)=InverseTauQPTFock;
        ScatteringRatesPTCoherent(idx1)=InverseTauQPTCoherent;
ScatteringRatesPTHighT(idx1)=InverseTauQPTHighT;
ScatteringRatesPTHighTNaive(idx1)=InverseTauQPTHighTNaive;
ScatteringRatesPTLowT(idx1)=InverseTauQPTLowT;
ScatteringRatesPTLowTInel(idx1)=InverseTauQPTLowTInel;
%         end
        
    end
end

% TRatioList=.1*10.^((4+log10(5))*(SweepParameter1List-1)/(30-1))/TDebye;
% TRatioList=3.*10.^((3.0)*(SweepParameter1List-1)/(30-1))/TDebye;
TRatioList=SweepParameter1List;


%data fitting for ScatteringRates
FitMin=find(TRatioList>3,1);
linearfitGamma=polyfit(log(TRatioList(FitMin:end)),log(ScatteringRates(FitMin:end)),1);%fitting upto FitMax-th savedtimestep
GammaSlope=linearfitGamma(1);
GammaIntercept=linearfitGamma(2);
linearfnGamma=exp(GammaSlope*log(TRatioList)+GammaIntercept);

linearfitGammaPT=polyfit(log(TRatioList(FitMin:end)),log(ScatteringRatesPT(FitMin:end)),1);%fitting upto FitMax-th savedtimestep
GammaSlopePT=linearfitGammaPT(1);
GammaInterceptPT=linearfitGammaPT(2);
linearfnGammaPT=exp(GammaSlopePT*log(TRatioList)+GammaInterceptPT);

% figure('Position',[0 0 1300 650])
% tiledlayout(1,2)
% nexttile;
% plot(TRatioList,ScatteringRates,'DisplayName','numerical','LineWidth',2)
% hold on
% plot(TRatioList,ScatteringRatesPT,'DisplayName','PT','LineWidth',2)
% % plot(TRatioList(round(numel(TRatioList)*.3):end),ScatteringRatesPTHighT(round(numel(TRatioList)*.3):end),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off') %,'HandleVisibility','off' is for removing legend %,'DisplayName','PTHighT'
% % plot(TRatioList(round(numel(TRatioList)*.3):end),ScatteringRatesPTHighTNaive(round(numel(TRatioList)*.3):end),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off') %,'HandleVisibility','off' is for removing legend %,'DisplayName','PTHighT'
% % plot(TRatioList(1:round(numel(TRatioList)*.2)),ScatteringRatesPTLowT(1:round(numel(TRatioList)*.2)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off') %,'DisplayName','PTLowT'
% hold off
% legend('Location','southeast')
% xlabel('T/T_D')
% ylabel('1/\tau')
% xlim([min(TRatioList) max(TRatioList)])
% ylim([-inf max(ScatteringRates)*1.1])
% title(['2k_F/q_D=',num2str(kFToqDover2)])
% 
% nexttile

ResistivityPT=m/e^2/(8.491e28)*ScatteringRatesPT*1e8;
ResistivityPTFock=m/e^2/(8.491e28)*ScatteringRatesPTFock*1e8;
ResistivityPTCoherent=m/e^2/(8.491e28)*ScatteringRatesPTCoherent*1e8;

Texpt=[20,25,30,35,40,45,50,55,60,70,80,90,100,125,150,175,200,225,250,273.15,293,300,350,400,500,600,700,800,900,1000,1100,1200,1300,1357.6];
ResistivityExpt=[.000798,.00249,.00628,.0127,.0219,.0338,.0498,.0707,.0951,.152,.213,.279,.346,.520,.697,.872,1.044,1.215,1.385,1.541,1.676,1.723,2.061,2.400,3.088,3.790,4.512,5.260,6.039,6.856,7.715,8.624,9.590,10.169];

%%
figure('Position',[0 0 650 650])
% loglog(TRatioList,ScatteringRates*1e50,'DisplayName','Numerical','LineWidth',2,'HandleVisibility','off')

loglog(TRatioList,ScatteringRatesPTFock,'Color',[0, 0.4470, 0.7410],'DisplayName','Q','LineWidth',3)



% 
% 
hold on
loglog(TRatioList,ScatteringRatesPTCoherent,'DisplayName','C','LineWidth',3)
loglog(TRatioList,ScatteringRatesPT,'Color',[0, 0.5, 0],'DisplayName','C_0','LineWidth',3)
% hold on

% loglog(Texpt,ResistivityExpt,'.','LineWidth',2)

% loglog(TRatioList,ScatteringRatesPTFock,'DisplayName','PTFock','LineWidth',2)
% loglog(TRatioList(round(numel(TRatioList)*.3):end),ScatteringRatesPTHighT(round(numel(TRatioList)*.3):end),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1,'HandleVisibility','off')%,'DisplayName','PTHighT'
loglog(TRatioList(round(numel(TRatioList)*.6):end),ScatteringRatesPTHighTNaive(round(numel(TRatioList)*.6):end),'--','Color',[0, 0, 0],'LineWidth',2,'HandleVisibility','off')%,'DisplayName','PTHighT'
loglog(TRatioList(1:round(numel(TRatioList)*.4)),ScatteringRatesPTLowT(1:round(numel(TRatioList)*.4)),'--','Color',[0, 0, 0],'LineWidth',2,'HandleVisibility','off')%,'DisplayName','PTLowT'
% loglog(TRatioList(1:round(numel(TRatioList)*.4)),ScatteringRatesPTLowTInel(1:round(numel(TRatioList)*.4)),'k--','LineWidth',1,'HandleVisibility','off')%,'DisplayName','PTLowT'
% loglog(TRatioList(round(numel(TRatioList)*.02):end),linearfnGamma(round(numel(TRatioList)*.02):end),'--','Color',[0 0.4470 0.7410],'DisplayName',['numerical HighT fitting',newline,'slope=',num2str(GammaSlope)],'LineWidth',1)
% loglog(TRatioList(round(numel(TRatioList)*.02):end),linearfnGammaPT(round(numel(TRatioList)*.02):end),'--','Color',[0.8500 0.3250 0.0980],'DisplayName',['PT HighT fitting',newline,'slope=',num2str(GammaSlopePT)],'LineWidth',1)
hold off
set(gca,'FontSize',FontSizeOverall)
legend('Location','northwest','FontSize',FontSizeAxes);
xlabel('T/T_D','FontSize',FontSizeAxes)
ylabel('1/\tau (s^{-1})','FontSize',FontSizeAxes)
% title(['2k_F/q_D=',num2str(kFToqDover2)],'FontSize',FontSizeAxes)
text(11e-3,7e8,'~T^5','Color','black','FontSize',FontSizeAxes)
% text(1.5e-1,2e10,'~T^4','Color','black','FontSize',FontSizeAxes)
text(.15e1,3.5e14,'~T','Color','black','FontSize',FontSizeAxes)
xlim([min(TRatioList) max(TRatioList)])
% ylim([-inf max(ResistivityPT)*1.1])
% 
% pbaspect([log(max(TRatioList))-log(min(TRatioList)) log(max(ResistivityPT))-log(min(ResistivityPT)) 1])
pbaspect([1 1 1])
% xlim([.1 5e3])
% ylim([1e-4 1e2])
% pbaspect([4+log10(5) 6 1])
%%
figure('Position',[0 0 400 440])
semilogx(TRatioList,ScatteringRatesPTFock./ScatteringRatesPTFock,'Color',[0, 0.4470, 0.7410],'DisplayName','Q/Q','LineWidth',2)

hold on
semilogx(TRatioList,ScatteringRatesPTCoherent./ScatteringRatesPTFock,'Color',[0.8500, 0.3250, 0.0980],'DisplayName','C/Q','LineWidth',2)
semilogx(TRatioList,ScatteringRatesPT./ScatteringRatesPTFock,'Color',[0, 0.5, 0],'DisplayName','C_0/Q','LineWidth',2)

hold off
set(gca,'FontSize',FontSizeOverall)
legend('Location','southeast','FontSize',FontSizeAxes)
xlabel('T/T_D','FontSize',FontSizeAxes)
xlim([-Inf 3*10^(3.5)/343])
% ylim([-Inf 1.05])

pbaspect([1 1 1])

% %%
% figure('Position',[0 0 650 650])
% loglog(TRatioList,VrmsList,'Color',[0.8500, 0.3250, 0.0980],'DisplayName','Vrms','LineWidth',2)
% 
% set(gca,'FontSize',FontSizeOverall)
% legend('Location','northeast','FontSize',FontSizeAxes)
% xlabel('T/T_D','FontSize',FontSizeAxes)
% % ylim([0 Inf])
% 
% %%
% figureA('Position',[0 0 650 650])
% semilogx(TRatioList,gradient(log(VrmsList),log(TRatioList(2)/TRatioList(1))),'Color',[0.8500, 0.3250, 0.0980],'DisplayName','VrmsSlope','LineWidth',2)
% 
% set(gca,'FontSize',FontSizeOverall)
% legend('Location','northeast','FontSize',FontSizeAxes)
% xlabel('T/T_D','FontSize',FontSizeAxes)
% % ylim([0 Inf])



%%
Vrms=VrmsAnal;
figure
delta=.2*Vrms;
humm=-Vrms*4:delta:Vrms*4;
plot(humm/e*1e3,erfc(humm/Vrms/sqrt(2)),'Color',[0.4660, 0.6740, 0.1880]	,'DisplayName',['T=',num2str(T),'K'],'LineWidth',3)  %classical DOS
% xlabel('Energy (eV)','Interpreter','Latex','FontSize',FontSizeAxes)
% xlim([-.15 .07])

xlabel('Energy (eV)','Interpreter','Latex','FontSize',FontSizeAxes)
xlim([-100 50])

ylabel('DOS (arb. units)','Interpreter','Latex','FontSize',FontSizeAxes)
legend()
return;
